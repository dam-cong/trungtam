# -*- coding: utf-8 -*-

from odoo import models, fields, api


class Congtyphaicu(models.Model):
    _name = 'nganhnghe'
    _rec_name = 'name'

    name = fields.Char(string='Tiếng Việt')
    name_en = fields.Char(string='Tiếng Anh')
    name_jp = fields.Char(string='Tiếng Nhật')
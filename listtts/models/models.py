# -*- coding: utf-8 -*-

from odoo import models, fields, api
import datetime


class department(models.Model):
    _name = 'department'
    _rec_name = 'name'

    name = fields.Char('Tên phòng phát triển thị trường')
    nghiepdoan = fields.Many2many(comodel_name="nghiepdoan", string="Danh sách nghiệp đoàn",
                                  compute='danhsachnghiepdoan')

    @api.multi
    def danhsachnghiepdoan(self):
        self.nghiepdoan = self.env['nghiepdoan'].search([('room_pttt', '=', self.id)])


class listtts(models.Model):
    _name = 'listtts.listtts'
    _rec_name = 'name'

    avatar = fields.Binary(string="avatar")
    custom_id = fields.Char(string='Mã số :')
    name = fields.Char(string='Họ và tên')
    name_in_japan = fields.Char(string='Họ tên tiếng nhật')
    date_of_birth = fields.Date(string='Ngày sinh')
    gender = fields.Selection(string="Giới tính",
                              selection=[('1', 'Nam'), ('2', 'Nữ')], required=False, )

    address = fields.Char(string='Địa chỉ liên hệ')
    province = fields.Char(string='Tỉnh/TP')
    phone = fields.Char(string='Số điện thoại')
    cmnd = fields.Char(string='CMND')
    idcard = fields.Char(string='Thẻ căn cước')
    date_license = fields.Date(string='Ngày cấp')
    address_license = fields.Char(string='Nơi cấp')
    status_mary = fields.Selection(string="Tình trạng hôn nhân",
                                   selection=[('1', 'Độc thân'), ('2', 'Kết hôn'), ('3', 'Ly hôn')], required=False, )
    level = fields.Selection(string="Trình độ học vấn",
                             selection=[('1', 'Bằng Tốt Nghiệp THCS'), ('2', 'Bằng Tốt Nghiệp THPT'),
                                        ('3', 'Bằng Tốt Nghiệp Trung Cấp'), ('4', 'Bằng Tốt Nghiệp Cao Đẳng'),
                                        ('5', 'Bằng Tốt Nghiệp Đại Học'), ('6', 'Bằng Thạc Sĩ')],
                             required=False, )
    phone_parent = fields.Char(string='Số điện thoại người thân')
    relative_note = fields.Char(string='Quan hệ')
    place_to_work = fields.Char(string='Địa điểm làm việc')
    job_jp = fields.Many2one(comodel_name='nganhnghe', string='Ngành nghề')
    age = fields.Char(string='Tuổi', compute='_tuoi_tts')

    lop_hoc = fields.Many2one('taolop.taolop')
    chuyenlop = fields.Boolean('Chuyển lớp')

    @api.multi
    @api.depends('date_of_birth')
    def _tuoi_tts(self):
        ngay_sinh = fields.Date.today()
        if self.date_of_birth:
            tmp = ngay_sinh.year - self.date_of_birth.year
            if ngay_sinh.month == self.date_of_birth.month:
                if ngay_sinh.day < self.date_of_birth.day:
                    tmp = tmp - 1
            elif ngay_sinh.month < self.date_of_birth.month:
                tmp = tmp - 1
            self.age = int(tmp)

    department = fields.Many2one(comodel_name='department', string='Phòng ban')

    @api.multi
    @api.onchange('department')
    def domain_department(self):
        if self.department:
            return {'domain': {'nghiepdoan': [('room_pttt', '=', self.department.id)]}}

    nghiepdoan = fields.Many2one(comodel_name='nghiepdoan', string='Nghiệp đoàn')

    @api.multi
    @api.onchange('nghiepdoan')
    def domain_nghiepdoan(self):
        if self.nghiepdoan:
            return {'domain': {'xinghiep': [('nghiepdoan', '=', self.nghiepdoan.id)]}}

    xinghiep = fields.Many2one(comodel_name='xinghiep', string='Xí nghiệp')
    date_departure = fields.Date('Dự kiến xuất cảnh')

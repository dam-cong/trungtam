
from odoo import models, fields, api
import datetime

class listnghiepdoan(models.Model):
    _name = 'nghiepdoan'
    _rec_name = 'name_roma'

    name_roma = fields.Char(string='Tên viết tắt - chữ Romaji')
    name_english = fields.Char(string='Tên Tiếng Anh')
    name_china = fields.Char(string='Tên đầy đủ - chữ Hán')
    note_viet = fields.Char(string='Ghi chú tiếng Việt')
    room_pttt = fields.Many2one(comodel_name="department", string="Phòng PTTT", required=False, )
    xinghiep = fields.Many2many(comodel_name="xinghiep", string="Danh sách xí nghiệp",compute='danhsachxinghiep')

    @api.multi
    def danhsachxinghiep(self):
        self.xinghiep = self.env['xinghiep'].search([('nghiepdoan', '=', self.id)])
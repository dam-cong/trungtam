from odoo import models, fields, api

class mydocument(models.Model):
    _name = 'intern.document'
    _description = u'Các loại văn bản, báo cáo'
    _rec_name = 'name'

    name = fields.Char("Tên", required=True)  # hh_104

    note = fields.Text("Ghi chú")  # hh_105
    attachment = fields.Binary('Văn bản mẫu', required=True)  # hh_106
# -*- coding: utf-8 -*-

from odoo import models, fields, api
import datetime

class listxinghiep(models.Model):
    _name = 'xinghiep'
    _rec_name = 'name_romaji'

    name_romaji = fields.Char(string='Tên xí nghiệp - tiếng Romaji')
    name_china = fields.Char(string='Tên xí nghiệp - tiếng Hán')
    address_china = fields.Char(string='Địa chỉ làm việc-tiếng Hán')
    address_romaji = fields.Char(string='Địa chỉ làm việc-tiếng Romaji')
    person_japan = fields.Char(string='Tên người đại diện - Tiếng Nhât')
    person_english = fields.Char(string='Tên người đại diện - Tiếng Anh')

    nghiepdoan = fields.Many2one(comodel_name="nghiepdoan", string="Nghiệp Đoàn", required=False, )

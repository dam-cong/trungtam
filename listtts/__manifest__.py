# -*- coding: utf-8 -*-
{
    'name': "listtts",
    'depends': ['base'],
    'data': [
        'security/groups_user.xml',
        'security/ir.model.access.csv',
        'views/views.xml',
        'views/phongnb.xml',
        'views/nghiepdoan.xml',
        'views/xinghiep.xml',
        'views/nganhnghe.xml',
        'views/document.xml',
    ],
}
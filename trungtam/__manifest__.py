# -*- coding: utf-8 -*-
{
    'name': "Trung tam",
    'depends': ['base','listtts'],
    'data': [
        'security/base_security.xml',
        'security/ir.model.access.csv',
        'views/taolop.xml',
        'views/baocaodiem.xml',
        'views/baocaodiemtt.xml',
        'views/nhapdiem.xml',
        'views/diemtheotuan.xml',
        'views/diemtrungtam.xml',
        'views/dsnhapdiem.xml',
        'views/loptts.xml',
        'views/thuctapsinhlop.xml',
        'views/xuatbaocao.xml',

        #
        # 'views/baihoc.xml',
        # 'views/danhgia.xml',
        # 'views/lydodanhgia.xml',
        # 'views/danhgiavolong.xml',
        # 'views/nhanxet.xml',
        # 'views/nhanxetxau.xml',
        # 'views/ythuc.xml',
        # 'views/ythucxau.xml',
        'views/mailtudong.xml'
    ],
}

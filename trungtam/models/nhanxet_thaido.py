# -*- coding: utf-8 -*-

from odoo import models, fields, api


class ThaiDo(models.Model):
    _name = 'thaido.thaido'
    _rec_name = "thoigian"

    thang = fields.Selection(string="Tháng", selection=[('1', 'Tháng 1'), ('2', 'Tháng 2'), ('3', 'Tháng 3'),
                                                        ('4', 'Tháng 4'), ('5', 'Tháng 5'), ('6', 'Tháng 6'),
                                                        ('7', 'Tháng 7'), ('8', 'Tháng 8'), ('9', 'Tháng 9'),
                                                        ('10', 'Tháng 10'), ('11', 'Tháng 11'),
                                                        ('12', 'Tháng 12')])
    nam_thaido = fields.Char(string='Năm', default='2020')
    thoigian = fields.Selection(string="Thời gian",
                                selection=[('A', 'A'), ('B', 'B'), ('C', 'C'), ('D', 'D'), ('E', 'E'), ('F', 'F')])
    tinhthan = fields.Selection(string="Sức Khỏe",
                                selection=[('A', 'A'), ('B', 'B'), ('C', 'C'), ('D', 'D'), ('E', 'E'), ('F', 'F')])
    kiluat = fields.Selection(string="Kỷ luật",
                              selection=[('A', 'A'), ('B', 'B'), ('C', 'C'), ('D', 'D'), ('E', 'E'), ('F', 'F')])
    nhanxet_hoctaptot = fields.Many2many(comodel_name='nhanxet', string='Nhận xét học tập (tốt)')
    nhanxet_hoctapxau = fields.Many2many(comodel_name='nhanxetxau', string="Nhận xét học tập (xấu)")
    nhanxet_ythuctot = fields.Many2many(comodel_name='nhanxet.ythuc', string="Nhận xét ý thức(tốt)")
    nhanxet_ythucxau = fields.Many2many(comodel_name='nhanxet.ythucxau', string="Nhận xét ý thức(xấu)")
    vang_danhgia = fields.Integer(string="Vắng mặt")
    lydo_danhgia = fields.Many2many(comodel_name='lydodanhgia', string='Lý do')
    danhgia_danhgia = fields.Many2many(comodel_name='danhgia', string='Đánh giá')
    thaido = fields.Many2one(comodel_name="lop.thuctapsinh")
    sequence = fields.Integer('sequence', help='Sequence for the handle.')

class nhanxet_n5(models.Model):
    _name = 'nhanxet'
    _rec_name = 'name'

    name = fields.Char('Nhận xét')
    sequence = fields.Integer('sequence', help='Sequence for the handle.')


class nhanxet_n5_xau(models.Model):
    _name = 'nhanxetxau'
    _rec_name = 'name'

    name = fields.Char('Nhận xét')
    sequence = fields.Integer('sequence', help='Sequence for the handle.')

class YThuc(models.Model):
    _name = 'nhanxet.ythuc'
    _rec_name = 'name'

    name = fields.Char('Nhận xét')
    sequence = fields.Integer('sequence', help='Sequence for the handle.')

class YThucXau(models.Model):
    _name = 'nhanxet.ythucxau'
    _rec_name = 'name'

    name = fields.Char('Nhận xét')
    sequence = fields.Integer('sequence', help='Sequence for the handle.')
# -*- coding: utf-8 -*-

from odoo import models, fields, api
from tempfile import TemporaryFile, NamedTemporaryFile
from odoo.exceptions import ValidationError, except_orm
from docxtpl import DocxTemplate, Listing
from datetime import datetime, timedelta
import codecs
from io import BytesIO, StringIO


def date_time_in_jp(day=None, month=None, year=None):
    if not day:
        if not month:
            return u'%s年' % year
        else:
            return u'%s年%s月' % (year, month)
    else:
        return u'%s年%s月%s日' % (year, month, day)


class NhapDiem(models.Model):
    _name = 'nhapdiem.nhapdiem'
    _rec_name = 'tenlop_nhapdiem'
    _order = 'tenlop_nhapdiem asc, nam_nhapdiem desc, thang_nhapdiem desc,tuan_nhapdiem asc'

    #
    @api.multi
    def _domain_lop(self):
        return [('giaovienchunhiem', '=', self.env.uid)]

    tenlop_nhapdiem = fields.Many2one(comodel_name='taolop.taolop', string='Tên lớp', domain=_domain_lop)
    trungtam_nhapdiem = fields.Many2one(comodel_name='trungtam.daotao', string='Trung tâm đào tạo',
                                        related='tenlop_nhapdiem.trungtam_taolop')
    giaovienchunhiem_nhapdiem = fields.Many2one('res.users', 'GVCN',
                                                related='tenlop_nhapdiem.giaovienchunhiem')
    volong_nhapdiem = fields.Boolean('Vỡ lòng')
    thang_nhapdiem = fields.Selection(string="Tháng", selection=[('1', 'Tháng 1'), ('2', 'Tháng 2'), ('3', 'Tháng 3'),
                                                                 ('4', 'Tháng 4'), ('5', 'Tháng 5'), ('6', 'Tháng 6'),
                                                                 ('7', 'Tháng 7'), ('8', 'Tháng 8'), ('9', 'Tháng 9'),
                                                                 ('10', 'Tháng 10'), ('11', 'Tháng 11'),
                                                                 ('12', 'Tháng 12'), ('13', 'Vỡ lòng')])
    tuan_nhapdiem = fields.Selection(string="Tuần", selection=[('1', 'Tuần 1'), ('2', 'Tuần 2'), ('3', 'Tuần 3'),
                                                               ('4', 'Tuần 4'), ('5', 'Vỡ lòng')])
    nam_nhapdiem = fields.Char(string="Năm")
    baihoc_nhapdiem = fields.Many2one('baihoc', 'Bài học')

    danhsach_thuctapsinh_volong = fields.One2many(comodel_name='danhsach.nhapdiem',
                                                  string='Danh sách thực tập sinh vỡ lòng',
                                                  inverse_name='danhsach_tts')
    danhsach_thuctapsinh = fields.One2many(comodel_name='danhsach.nhapdiem', string='Danh sách thực tập sinh',
                                           inverse_name='danhsach_tts')
    demo = fields.Boolean()
    tuan = fields.Date(string="Ngày tạo", required=True, default=fields.Date.today())

    @api.onchange('volong_nhapdiem')
    def onchange_method(self):
        if self.volong_nhapdiem == True:
            self.thang_nhapdiem = '13'
            self.tuan_nhapdiem = '5'

    @api.multi
    @api.onchange('tenlop_nhapdiem')
    def onchange_tenlop_nhapdiem_abc(self):
        for danhsach in self:
            for tts in danhsach.danhsach_thuctapsinh:
                if tts.tong == 0 or tts.tong == False:
                    tts.chua_codiem = True
                elif tts.tong > 0:
                    tts.chua_codiem = False

    @api.model
    def create(self, vals):
        danhsach = super(NhapDiem, self).create(vals)
        for tts in danhsach.danhsach_thuctapsinh:
            if tts.tong == 0 or tts.tong == False:
                tts.chua_codiem = True
            elif tts.tong > 0:
                tts.chua_codiem = False
        return danhsach

    @api.multi
    def write(self, vals):
        phanlop = super(NhapDiem, self).write(vals)
        for tts in self.danhsach_thuctapsinh:
            if tts.tong == 0 or tts.tong == False:
                tts.chua_codiem = True
            elif tts.tong > 0:
                tts.chua_codiem = False

        if self.danhsach_thuctapsinh_volong:
            for record in self.danhsach_thuctapsinh_volong:
                record.write({'baihoc_danhgia': self.baihoc_nhapdiem.id})
        elif self.danhsach_thuctapsinh:
            for record in self.danhsach_thuctapsinh:
                record.write({'baihoc_danhgia': self.baihoc_nhapdiem.id})
        return phanlop

    @api.multi
    def baocao_theotuan_button(self):
        return {
            'type': 'ir.actions.act_url',
            'url': '/baocao_theotuan_button/%s' % (self.id),
            'target': 'self', }

    def baocao_theotuan(self, document):
        docs = self.env['intern.document'].search([('name', '=', "CSDT")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].attachment, "base64"))
            docdata = DocxTemplate(stream)
            context = {}
            context[
                'csdt_0020'] = document.baihoc_nhapdiem.baihocdanhgia if document.baihoc_nhapdiem.baihocdanhgia else ''
            context[
                'csdt_0003'] = document.giaovienchunhiem_nhapdiem.partner_id.name if document.giaovienchunhiem_nhapdiem.partner_id.name else ''
            context[
                'csdt_0001'] = document.tenlop_nhapdiem.tenlop_taolop if document.tenlop_nhapdiem.tenlop_taolop else ''
            table_daotao_tuan = []
            for danhgia_nangluc_tuan in document.danhsach_thuctapsinh:
                infor = {}
                infor[
                    'hhjp_0001'] = danhgia_nangluc_tuan.tentss_danhsach.name if danhgia_nangluc_tuan.tentss_danhsach.name else ''
                infor[
                    'csdt_0047'] = danhgia_nangluc_tuan.tuvung_1 if danhgia_nangluc_tuan.tuvung_1 else '*'
                infor[
                    'csdt_0051'] = danhgia_nangluc_tuan.nguphap if danhgia_nangluc_tuan.nguphap else '*'
                infor[
                    'csdt_0023'] = danhgia_nangluc_tuan.nghe if danhgia_nangluc_tuan.nghe else '*'
                infor[
                    'csdt_0024'] = danhgia_nangluc_tuan.hoithoai if danhgia_nangluc_tuan.hoithoai else '*'
                infor[
                    'csdt_0025'] = danhgia_nangluc_tuan.tiengnhat if danhgia_nangluc_tuan.tiengnhat else '*'
                infor[
                    'csdt_0055'] = danhgia_nangluc_tuan.tong if danhgia_nangluc_tuan.tong else '*'
                infor['csdt_0027'] = danhgia_nangluc_tuan.danhgia_hoctap if danhgia_nangluc_tuan.danhgia_hoctap else '*'
                table_daotao_tuan.append(infor)
            context['tbl_baihoc_tuan'] = table_daotao_tuan
            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    def baocao_theotuan_volong(self, document):
        docs = self.env['intern.document'].search([('name', '=', "CSDT1")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].attachment, "base64"))
            docdata = DocxTemplate(stream)
            context = {}

            if document.volong_nhapdiem:
                context['csdt_0056'] = u'Vỡ lòng'
            context[
                'csdt_0003'] = document.giaovienchunhiem_nhapdiem.partner_id.name if document.giaovienchunhiem_nhapdiem.partner_id.name else ''
            context[
                'csdt_0001'] = document.tenlop_nhapdiem.tenlop_taolop if document.tenlop_nhapdiem.tenlop_taolop else ''
            table_daotao_volong_tuan = []
            for danhgia_nangluc_volong_tuan in document.danhsach_thuctapsinh_volong:
                infor = {}
                infor[
                    'hhjp_0001'] = danhgia_nangluc_volong_tuan.tentss_danhsach.name if danhgia_nangluc_volong_tuan.tentss_danhsach.name else ''
                infor[
                    'csdt_0004'] = danhgia_nangluc_volong_tuan.tuvung_volong if danhgia_nangluc_volong_tuan.tuvung_volong else '*'
                infor[
                    'csdt_0005'] = danhgia_nangluc_volong_tuan.nguphap_volong if danhgia_nangluc_volong_tuan.nguphap_volong else '*'
                infor[
                    'csdt_0009'] = danhgia_nangluc_volong_tuan.tong_volong if danhgia_nangluc_volong_tuan.tong_volong else '*'

                danhgia_volong = ''
                for item in danhgia_nangluc_volong_tuan.danhsach_thuctapsinh_volong.danhgia_volong:
                    danhgia_volong = danhgia_volong + '\n - %s' % (item.name)
                infor['csdt_0016'] = Listing(danhgia_volong)
                table_daotao_volong_tuan.append(infor)
            context['tbl_baihoc_volong_tuan'] = table_daotao_volong_tuan
            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    @api.multi
    def baocao_theoquatrinh_button(self):
        return {
            'type': 'ir.actions.act_url',
            'url': '/baocao_theoquatrinh_button/%s' % (self.id),
            'target': 'self', }

    def baocao_hoso_quatrinh(self, document, dataid):
        docs = self.env['intern.document'].search([('name', '=', "InfoTTS")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].attachment, "base64"))
            docdata = DocxTemplate(stream)
            data = self.env['danhsach.nhapdiem'].search([('id', '=', dataid)], limit=1)
            context = {}
            #
            if data.danhsach_thuctapsinh.tentss_thuctapsinh.avatar is not None:
                try:
                    streamAvatar = BytesIO(
                        data.danhsach_thuctapsinh.tentss_thuctapsinh.avatar.decode("base64"))
                    docdata.replace_pic('avatar.jpg', streamAvatar)
                except:
                    pass

            context[
                'hhjp_0001'] = data.danhsach_thuctapsinh.tentss_thuctapsinh.name_in_japan if data.danhsach_thuctapsinh.tentss_thuctapsinh.name_in_japan else ''
            context[
                'hhjp_0097'] = data.danhsach_thuctapsinh.nghiepdoan_tiepnhan.name_china if data.danhsach_thuctapsinh.nghiepdoan_tiepnhan.name_china else ''
            context[
                'hhjp_0010'] = data.danhsach_thuctapsinh.xinghiep_tiepnhan.name_china if data.danhsach_thuctapsinh.xinghiep_tiepnhan.name_china else ''
            context['hhjp_0115'] = data.danhsach_thuctapsinh.job_jp if data.danhsach_thuctapsinh.job_jp else ''
            # Ngày tháng năm sinh
            if data.danhsach_thuctapsinh.tentss_thuctapsinh.date_of_birth:
                context['hhjp_0004'] = date_time_in_jp(data.danhsach_thuctapsinh.tentss_thuctapsinh.date_of_birth.day,
                                                       data.danhsach_thuctapsinh.tentss_thuctapsinh.date_of_birth.month,
                                                       data.danhsach_thuctapsinh.tentss_thuctapsinh.date_of_birth.year)
            else:
                context['hhjp_0004'] = u'年　月　日'
            context[
                'csdt_0001'] = data.danhsach_thuctapsinh.tenlop_thuctapsinh.tenlop_taolop if data.danhsach_thuctapsinh.tenlop_thuctapsinh.tenlop_taolop else ''
            context[
                'csdt_0003'] = data.danhsach_thuctapsinh.chunhiem_lop.partner_id.name if data.danhsach_thuctapsinh.chunhiem_lop.partner_id.name else ''
            # # context['hhjp_0115'] = document.

            if data.danhsach_thuctapsinh.batdau_lop:
                context['csdt_0002'] = date_time_in_jp(data.danhsach_thuctapsinh.batdau_lop.day,
                                                       data.danhsach_thuctapsinh.batdau_lop.month,
                                                       data.danhsach_thuctapsinh.batdau_lop.year)
            else:
                context['csdt_0002'] = u'年　月　日'
            if data.danhsach_thuctapsinh.lichhoc_lop:
                context['hhjp_0902'] = date_time_in_jp(data.danhsach_thuctapsinh.lichhoc_lop.day,
                                                       data.danhsach_thuctapsinh.lichhoc_lop.month,
                                                       data.danhsach_thuctapsinh.lichhoc_lop.year)
            else:
                context['hhjp_0902'] = u'年　月　日'

            diem_volong = 0
            sosanh = 0
            do = []
            khongdo = []
            for tts_do in data.danhsach_thuctapsinh.danhsach_thuctapsinh_volong:
                if tts_do.do_volong == True:
                    do.append(tts_do)
                else:
                    khongdo.append(tts_do)
            if len(do) > 0:
                for volong in do:
                    sosanh = 0 + diem_volong
                    diem_volong = volong.tong_volong
                    if diem_volong >= sosanh:
                        context['csdt_0004'] = volong.tuvung_volong if volong.tuvung_volong else '*'
                        context['csdt_0005'] = volong.nguphap_volong if volong.nguphap_volong else '*'
                        context['csdt_0009'] = volong.tong_volong if volong.tong_volong else '*'
                        context['csdt_0010'] = u'✔'
                        context['csdt_0011'] = u''
            else:
                if len(khongdo) > 0:
                    for volong in khongdo:
                        sosanh = 0 + diem_volong
                        diem_volong = volong.tong_volong
                        if diem_volong >= sosanh:
                            context['csdt_0004'] = volong.tuvung_volong if volong.tuvung_volong else '*'
                            context['csdt_0005'] = volong.nguphap_volong if volong.nguphap_volong else '*'
                            context['csdt_0009'] = volong.tong_volong if volong.tong_volong else '*'
                            context['csdt_0010'] = u''
                            context['csdt_0011'] = u'✔'

            context[
                'csdt_0012'] = data.danhsach_thuctapsinh.thoigian_danhgia if data.danhsach_thuctapsinh.thoigian_danhgia else '*'
            context[
                'csdt_0013'] = data.danhsach_thuctapsinh.suckhoe_danhgia if data.danhsach_thuctapsinh.suckhoe_danhgia else '*'
            context[
                'csdt_0014'] = data.danhsach_thuctapsinh.kiluat_danhgia if data.danhsach_thuctapsinh.kiluat_danhgia else '*'

            danhgia_ythuctot_volong = ''
            for item in data.danhsach_thuctapsinh.danhgia_ythuctot_volong:
                danhgia_ythuctot_volong = danhgia_ythuctot_volong + '\n' + u'・' + (item.name)

            danhgia_ythucxau_volong = ''
            for item in data.danhsach_thuctapsinh.danhgia_ythucxau_volong:
                danhgia_ythucxau_volong = danhgia_ythucxau_volong + '\n' + u'・' + (item.name)

            if danhgia_ythuctot_volong:
                danhgia = danhgia_ythuctot_volong
            elif danhgia_ythucxau_volong:
                danhgia = danhgia_ythucxau_volong
            elif danhgia_ythuctot_volong and danhgia_ythucxau_volong:
                danhgia = danhgia_ythuctot_volong + ' %s' % (danhgia_ythucxau_volong)
            else:
                danhgia = '*'

            context['csdt_0015'] = Listing(danhgia)

            danhgia_volong = ''
            for item in data.danhsach_thuctapsinh.danhgia_volong:
                danhgia_volong = danhgia_volong + u'・' + (item.name)
            context['csdt_0016'] = Listing(danhgia_volong)

            table_daotao = []
            thang = []
            for danhgia_nangluc in data.danhsach_thuctapsinh.thaido_tts_lop:
                if danhgia_nangluc.thang not in thang:
                    thang.append(danhgia_nangluc.thang)
                    infor_danhgia_nangluc = {}
                    infor_danhgia_nangluc[
                        'csdt_0019'] = danhgia_nangluc.thang if danhgia_nangluc.thang else ''
                    table_baihoc_thang = []
                    item = 1
                    for baihoc_thang in data.danhsach_thuctapsinh.danhgia_nangluc_tts_lop:
                        if baihoc_thang.thang_danhgia == danhgia_nangluc.thang:
                            if item == 1:
                                infor_danhgia_nangluc[
                                    'csdt_0020_1'] = baihoc_thang.baihoc_danhgia.baihocdanhgia if baihoc_thang.baihoc_danhgia.baihocdanhgia else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0021_1'] = baihoc_thang.tuvung_1 if baihoc_thang.tuvung_1 else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0022_1'] = baihoc_thang.nguphap if baihoc_thang.nguphap else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0023_1'] = baihoc_thang.nghe if baihoc_thang.nghe else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0024_1'] = baihoc_thang.hoithoai if baihoc_thang.hoithoai else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0025_1'] = baihoc_thang.tiengnhat if baihoc_thang.tiengnhat else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0026_1'] = baihoc_thang.tong if baihoc_thang.tong else '*'
                                item += 1;
                            elif item == 2:
                                infor_danhgia_nangluc[
                                    'csdt_0020_2'] = baihoc_thang.baihoc_danhgia.baihocdanhgia if baihoc_thang.baihoc_danhgia.baihocdanhgia else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0021_2'] = baihoc_thang.tuvung_1 if baihoc_thang.tuvung_1 else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0022_2'] = baihoc_thang.nguphap if baihoc_thang.nguphap else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0023_2'] = baihoc_thang.nghe if baihoc_thang.nghe else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0024_2'] = baihoc_thang.hoithoai if baihoc_thang.hoithoai else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0025_2'] = baihoc_thang.tiengnhat if baihoc_thang.tiengnhat else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0026_2'] = baihoc_thang.tong if baihoc_thang.tong else '*'
                                item += 1;
                            elif item == 3:
                                infor_danhgia_nangluc[
                                    'csdt_0020_3'] = baihoc_thang.baihoc_danhgia.baihocdanhgia if baihoc_thang.baihoc_danhgia.baihocdanhgia else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0021_3'] = baihoc_thang.tuvung_1 if baihoc_thang.tuvung_1 else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0022_3'] = baihoc_thang.nguphap if baihoc_thang.nguphap else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0023_3'] = baihoc_thang.nghe if baihoc_thang.nghe else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0024_3'] = baihoc_thang.hoithoai if baihoc_thang.hoithoai else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0025_3'] = baihoc_thang.tiengnhat if baihoc_thang.tiengnhat else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0026_3'] = baihoc_thang.tong if baihoc_thang.tong else '*'
                                item += 1;
                            elif item == 4:
                                infor_danhgia_nangluc[
                                    'csdt_0020_4'] = baihoc_thang.baihoc_danhgia.baihocdanhgia if baihoc_thang.baihoc_danhgia.baihocdanhgia else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0021_4'] = baihoc_thang.tuvung_1 if baihoc_thang.tuvung_1 else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0022_4'] = baihoc_thang.nguphap if baihoc_thang.nguphap else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0023_4'] = baihoc_thang.nghe if baihoc_thang.nghe else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0024_4'] = baihoc_thang.hoithoai if baihoc_thang.hoithoai else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0025_4'] = baihoc_thang.tiengnhat if baihoc_thang.tiengnhat else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0026_4'] = baihoc_thang.tong if baihoc_thang.tong else '*'
                                break

                    for thaido_tts_lop in data.danhsach_thuctapsinh.thaido_tts_lop:
                        if thaido_tts_lop.thang == danhgia_nangluc.thang:
                            infor_danhgia_nangluc[
                                'csdt_0028'] = thaido_tts_lop.thoigian if thaido_tts_lop.thoigian else '*'
                            infor_danhgia_nangluc[
                                'csdt_0029'] = thaido_tts_lop.tinhthan if thaido_tts_lop.tinhthan else '*'
                            infor_danhgia_nangluc['csdt_0030'] = thaido_tts_lop.kiluat if thaido_tts_lop.kiluat else '*'
                            infor_danhgia_nangluc[
                                'csdt_0017'] = thaido_tts_lop.vang_danhgia if thaido_tts_lop.vang_danhgia else ''

                            lydo_danhgia = ''
                            for item in thaido_tts_lop.lydo_danhgia:
                                lydo_danhgia = lydo_danhgia + u'・' + (item.name)
                            infor_danhgia_nangluc['csdt_0018'] = Listing(lydo_danhgia) if lydo_danhgia else '*'

                            nhanxet_hoctaptot = ''
                            for item in thaido_tts_lop.nhanxet_hoctaptot:
                                nhanxet_hoctaptot = nhanxet_hoctaptot + '\n' + u'・' + (item.name)
                            nhanxet_hoctapxau = ''
                            for item in thaido_tts_lop.nhanxet_hoctapxau:
                                nhanxet_hoctapxau = nhanxet_hoctapxau + '\n' + u'・' + (item.name)

                            if thaido_tts_lop.nhanxet_hoctaptot and thaido_tts_lop.nhanxet_hoctapxau:
                                danhgia_nangluc_1 = nhanxet_hoctaptot + '%s' % (nhanxet_hoctapxau)
                            elif thaido_tts_lop.nhanxet_hoctaptot:
                                danhgia_nangluc_1 = nhanxet_hoctaptot
                            elif thaido_tts_lop.nhanxet_hoctapxau:
                                danhgia_nangluc_1 = nhanxet_hoctapxau
                            else:
                                danhgia_nangluc_1 = '*'

                            infor_danhgia_nangluc['csdt_0027'] = Listing(danhgia_nangluc_1)

                            nhanxet_ythuctot = ''
                            for item in thaido_tts_lop.nhanxet_ythuctot:
                                nhanxet_ythuctot = nhanxet_ythuctot + '\n' + u'・' + (item.name)
                            nhanxet_ythucxau = ''
                            for item in thaido_tts_lop.nhanxet_ythucxau:
                                nhanxet_ythucxau = nhanxet_ythucxau + '\n' + u'・' + (item.name)

                            if thaido_tts_lop.nhanxet_ythuctot and thaido_tts_lop.nhanxet_ythucxau:
                                csdt_0031 = nhanxet_ythuctot + ' %s' % (nhanxet_ythucxau)
                            elif thaido_tts_lop.nhanxet_ythuctot:
                                csdt_0031 = nhanxet_ythuctot
                            elif thaido_tts_lop.nhanxet_ythucxau:
                                csdt_0031 = nhanxet_ythucxau
                            else:
                                csdt_0031 = '*'

                            infor_danhgia_nangluc['csdt_0031'] = Listing(csdt_0031)
                    table_daotao.append(infor_danhgia_nangluc)
            context['tbl_baihoc'] = table_daotao

            table_diem_n5 = []
            for i in data.danhsach_thuctapsinh.diem_n5:
                diem = {}
                if i.thang_danhgia_n5:
                    diem['csdt_0041'] = date_time_in_jp(i.thang_danhgia_n5.day,
                                                        i.thang_danhgia_n5.month,
                                                        i.thang_danhgia_n5.year)
                else:
                    diem['csdt_0041'] = u'年　月　日'
                diem['csdt_0032'] = i.tuvung_lop if i.tuvung_lop else ''
                diem['csdt_0033'] = i.doc_nguphap_lop if i.doc_nguphap_lop else ''
                diem['csdt_0034'] = i.nghe_hieu_lop if i.nghe_hieu_lop else ''
                diem['csdt_0035'] = i.hoi_thoai_lop if i.hoi_thoai_lop else ''
                diem['csdt_0036'] = i.tong_lop if i.tong_lop else ''
                diem['csdt_0039'] = i.danhgia_n5 if i.danhgia_n5 else ''
                if i.tong_lop or i.thang_danhgia_n5:
                    if i.do_lop == True:
                        diem['csdt_0037'] = u'✔'
                    else:
                        diem['csdt_0038'] = u'✔'
                else:
                    diem['csdt_0037'] = ''
                    diem['csdt_0038'] = ''
                table_diem_n5.append(diem)
            context['tbl_n5'] = table_diem_n5

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    def baocao_hoso_quatrinh_volong(self, document, volongid):
        docs = self.env['intern.document'].search([('name', '=', "InfoTTS")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].attachment, "base64"))
            docdata = DocxTemplate(stream)
            context = {}
            volong = self.env['danhsach.nhapdiem'].search([('id', '=', volongid)], limit=1)
            #
            if volong.danhsach_thuctapsinh_volong.tentss_thuctapsinh.avatar is not None:
                try:
                    streamAvatar = BytesIO(document.tentss_thuctapsinh.avatar.decode("base64"))
                    docdata.replace_pic('avatar.jpg', streamAvatar)
                except:
                    pass

            context[
                'hhjp_0001'] = volong.danhsach_thuctapsinh_volong.tentss_thuctapsinh.name_in_japan if volong.danhsach_thuctapsinh_volong.tentss_thuctapsinh.name_in_japan else ''
            context[
                'hhjp_0097'] = volong.danhsach_thuctapsinh_volong.nghiepdoan_tiepnhan.name_china if volong.danhsach_thuctapsinh_volong.nghiepdoan_tiepnhan.name_china else ''
            context[
                'hhjp_0010'] = volong.danhsach_thuctapsinh_volong.xinghiep_tiepnhan.name_china if volong.danhsach_thuctapsinh_volong.xinghiep_tiepnhan.name_china else ''
            context[
                'hhjp_0115'] = volong.danhsach_thuctapsinh_volong.job_jp if volong.danhsach_thuctapsinh_volong.job_jp else ''
            # Ngày tháng năm sinh
            if volong.danhsach_thuctapsinh_volong.tentss_thuctapsinh.day and volong.danhsach_thuctapsinh_volong.tentss_thuctapsinh.month and volong.danhsach_thuctapsinh_volong.tentss_thuctapsinh.year:
                context['hhjp_0004'] = date_time_in_jp(volong.danhsach_thuctapsinh_volong.tentss_thuctapsinh.day,
                                                       volong.danhsach_thuctapsinh_volong.tentss_thuctapsinh.month,
                                                       volong.danhsach_thuctapsinh_volong.tentss_thuctapsinh.year)
            else:
                context['hhjp_0004'] = u'年　月　日'
            context[
                'csdt_0001'] = volong.danhsach_thuctapsinh_volong.tenlop_thuctapsinh.tenlop_taolop if volong.danhsach_thuctapsinh_volong.tenlop_thuctapsinh.tenlop_taolop else ''
            context[
                'csdt_0003'] = volong.danhsach_thuctapsinh_volong.chunhiem_lop.partner_id.name if volong.danhsach_thuctapsinh_volong.chunhiem_lop.partner_id.name else ''

            if volong.danhsach_thuctapsinh_volong.batdau_lop:
                context['csdt_0002'] = date_time_in_jp(volong.danhsach_thuctapsinh_volong.batdau_lop.day,
                                                       volong.danhsach_thuctapsinh_volong.batdau_lop.month,
                                                       volong.danhsach_thuctapsinh_volong.batdau_lop.year)
            else:
                context['csdt_0002'] = u'年　月　日'

            if volong.danhsach_thuctapsinh_volong.lichhoc_lop:
                context['hhjp_0902'] = date_time_in_jp(volong.danhsach_thuctapsinh_volong.lichhoc_lop.day,
                                                       volong.danhsach_thuctapsinh_volong.lichhoc_lop.month,
                                                       volong.danhsach_thuctapsinh_volong.lichhoc_lop.year)
            else:
                context['hhjp_0902'] = u'年　月　日'

            diem_volong = 0
            sosanh = 0
            do = []
            khongdo = []
            for tts_do in volong.danhsach_thuctapsinh_volong.danhsach_thuctapsinh_volong:
                if tts_do.do_volong == True:
                    do.append(tts_do)
                else:
                    khongdo.append(tts_do)
            if len(do) > 0:
                for volong in do:
                    sosanh = 0 + diem_volong
                    diem_volong = volong.tong_volong
                    if diem_volong >= sosanh:
                        context['csdt_0004'] = volong.tuvung_volong if volong.tuvung_volong else '*'
                        context['csdt_0005'] = volong.nguphap_volong if volong.nguphap_volong else '*'
                        context['csdt_0009'] = volong.tong_volong if volong.tong_volong else '*'
                        context['csdt_0010'] = u'✔'
                        context['csdt_0011'] = u''
            else:
                if len(khongdo) > 0:
                    for volong in khongdo:
                        sosanh = 0 + diem_volong
                        diem_volong = volong.tong_volong
                        if diem_volong >= sosanh:
                            context['csdt_0004'] = volong.tuvung_volong if volong.tuvung_volong else '*'
                            context['csdt_0005'] = volong.nguphap_volong if volong.nguphap_volong else '*'
                            context['csdt_0009'] = volong.tong_volong if volong.tong_volong else '*'
                            context['csdt_0010'] = u''
                            context['csdt_0011'] = u'✔'

            context[
                'csdt_0012'] = volong.danhsach_thuctapsinh_volong.thoigian_danhgia if volong.danhsach_thuctapsinh_volong.thoigian_danhgia else '*'
            context[
                'csdt_0013'] = volong.danhsach_thuctapsinh_volong.suckhoe_danhgia if volong.danhsach_thuctapsinh_volong.suckhoe_danhgia else '*'
            context[
                'csdt_0014'] = volong.danhsach_thuctapsinh_volong.kiluat_danhgia if volong.danhsach_thuctapsinh_volong.kiluat_danhgia else '*'

            danhgia_ythuctot_volong = ''
            for item in volong.danhsach_thuctapsinh_volong.danhgia_ythuctot_volong:
                danhgia_ythuctot_volong = danhgia_ythuctot_volong + '\n - %s' % (item.name)

            danhgia_ythucxau_volong = ''
            for item in volong.danhsach_thuctapsinh_volong.danhgia_ythucxau_volong:
                danhgia_ythucxau_volong = danhgia_ythucxau_volong + '\n - %s' % (item.name)
            if danhgia_ythuctot_volong:
                danhgia = danhgia_ythuctot_volong
            elif danhgia_ythucxau_volong:
                danhgia = danhgia_ythucxau_volong
            elif danhgia_ythuctot_volong and danhgia_ythucxau_volong:
                danhgia = danhgia_ythuctot_volong + ' %s' % (danhgia_ythucxau_volong)
            else:
                danhgia = '*'

            context['csdt_0015'] = Listing(danhgia)

            danhgia_volong = ''
            for item in volong.danhsach_thuctapsinh_volong.danhgia_volong:
                danhgia_volong = danhgia_volong + '\n - %s' % (item.name)
            context['csdt_0016'] = Listing(danhgia_volong)

            table_daotao = []
            thang = []
            for danhgia_nangluc in volong.danhsach_thuctapsinh_volong.thaido_tts_lop:
                if danhgia_nangluc.thang not in thang:
                    thang.append(danhgia_nangluc.thang)
                    infor_danhgia_nangluc = {}
                    infor_danhgia_nangluc[
                        'csdt_0019'] = danhgia_nangluc.thang if danhgia_nangluc.thang else ''
                    table_baihoc_thang = []
                    item = 1
                    for baihoc_thang in volong.danhsach_thuctapsinh_volong.danhgia_nangluc_tts_lop:
                        if baihoc_thang.thang_danhgia == danhgia_nangluc.thang:
                            if item == 1:
                                infor_danhgia_nangluc[
                                    'csdt_0020_1'] = baihoc_thang.baihoc_danhgia.baihocdanhgia if baihoc_thang.baihoc_danhgia.baihocdanhgia else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0021_1'] = baihoc_thang.tuvung_1 if baihoc_thang.tuvung_1 else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0022_1'] = baihoc_thang.nguphap if baihoc_thang.nguphap else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0023_1'] = baihoc_thang.nghe if baihoc_thang.nghe else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0024_1'] = baihoc_thang.hoithoai if baihoc_thang.hoithoai else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0025_1'] = baihoc_thang.tiengnhat if baihoc_thang.tiengnhat else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0026_1'] = baihoc_thang.tong if baihoc_thang.tong else '*'
                                item += 1;
                            elif item == 2:
                                infor_danhgia_nangluc[
                                    'csdt_0020_2'] = baihoc_thang.baihoc_danhgia.baihocdanhgia if baihoc_thang.baihoc_danhgia.baihocdanhgia else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0021_2'] = baihoc_thang.tuvung_1 if baihoc_thang.tuvung_1 else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0022_2'] = baihoc_thang.nguphap if baihoc_thang.nguphap else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0023_2'] = baihoc_thang.nghe if baihoc_thang.nghe else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0024_2'] = baihoc_thang.hoithoai if baihoc_thang.hoithoai else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0025_2'] = baihoc_thang.tiengnhat if baihoc_thang.tiengnhat else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0026_2'] = baihoc_thang.tong if baihoc_thang.tong else '*'
                                item += 1;
                            elif item == 3:
                                infor_danhgia_nangluc[
                                    'csdt_0020_3'] = baihoc_thang.baihoc_danhgia.baihocdanhgia if baihoc_thang.baihoc_danhgia.baihocdanhgia else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0021_3'] = baihoc_thang.tuvung_1 if baihoc_thang.tuvung_1 else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0022_3'] = baihoc_thang.nguphap if baihoc_thang.nguphap else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0023_3'] = baihoc_thang.nghe if baihoc_thang.nghe else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0024_3'] = baihoc_thang.hoithoai if baihoc_thang.hoithoai else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0025_3'] = baihoc_thang.tiengnhat if baihoc_thang.tiengnhat else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0026_3'] = baihoc_thang.tong if baihoc_thang.tong else '*'
                                item += 1;
                            elif item == 4:
                                infor_danhgia_nangluc[
                                    'csdt_0020_4'] = baihoc_thang.baihoc_danhgia.baihocdanhgia if baihoc_thang.baihoc_danhgia.baihocdanhgia else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0021_4'] = baihoc_thang.tuvung_1 if baihoc_thang.tuvung_1 else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0022_4'] = baihoc_thang.nguphap if baihoc_thang.nguphap else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0023_4'] = baihoc_thang.nghe if baihoc_thang.nghe else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0024_4'] = baihoc_thang.hoithoai if baihoc_thang.hoithoai else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0025_4'] = baihoc_thang.tiengnhat if baihoc_thang.tiengnhat else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0026_4'] = baihoc_thang.tong if baihoc_thang.tong else '*'
                                break

                    for thaido_tts_lop in volong.danhsach_thuctapsinh_volong.thaido_tts_lop:
                        if thaido_tts_lop.thang == danhgia_nangluc.thang:
                            infor_danhgia_nangluc[
                                'csdt_0028'] = thaido_tts_lop.thoigian if thaido_tts_lop.thoigian else '*'
                            infor_danhgia_nangluc[
                                'csdt_0029'] = thaido_tts_lop.tinhthan if thaido_tts_lop.tinhthan else '*'
                            infor_danhgia_nangluc['csdt_0030'] = thaido_tts_lop.kiluat if thaido_tts_lop.kiluat else '*'
                            infor_danhgia_nangluc[
                                'csdt_0017'] = thaido_tts_lop.vang_danhgia if thaido_tts_lop.vang_danhgia else ''

                            lydo_danhgia = ''
                            for item in thaido_tts_lop.lydo_danhgia:
                                lydo_danhgia = lydo_danhgia + '\n - %s' % (item.name)
                            infor_danhgia_nangluc['csdt_0018'] = Listing(lydo_danhgia) if lydo_danhgia else '*'

                            nhanxet_hoctaptot = ''
                            for item in thaido_tts_lop.nhanxet_hoctaptot:
                                nhanxet_hoctaptot = nhanxet_hoctaptot + '\n - %s' % (item.name)
                            nhanxet_hoctapxau = ''
                            for item in thaido_tts_lop.nhanxet_hoctapxau:
                                nhanxet_hoctapxau = nhanxet_hoctapxau + '\n - %s' % (item.name)

                            if thaido_tts_lop.nhanxet_hoctaptot and thaido_tts_lop.nhanxet_hoctapxau:
                                danhgia_nangluc1 = nhanxet_hoctaptot + '%s' % (nhanxet_hoctapxau)
                            elif thaido_tts_lop.nhanxet_hoctaptot:
                                danhgia_nangluc1 = nhanxet_hoctaptot
                            elif thaido_tts_lop.nhanxet_hoctapxau:
                                danhgia_nangluc1 = nhanxet_hoctapxau
                            else:
                                danhgia_nangluc1 = '*'
                            infor_danhgia_nangluc['csdt_0027'] = Listing(danhgia_nangluc1)

                            nhanxet_ythuctot = ''
                            for item in thaido_tts_lop.nhanxet_ythuctot:
                                nhanxet_ythuctot = nhanxet_ythuctot + '\n- %s' % (item.name)

                            nhanxet_ythucxau = ''
                            for item in thaido_tts_lop.nhanxet_ythucxau:
                                nhanxet_ythucxau = nhanxet_ythucxau + '\n- %s' % (item.name)

                            if thaido_tts_lop.nhanxet_ythuctot and thaido_tts_lop.nhanxet_ythucxau:
                                csdt_0031 = nhanxet_ythuctot + '- %s' % (nhanxet_ythucxau)
                            elif thaido_tts_lop.nhanxet_ythuctot:
                                csdt_0031 = nhanxet_ythuctot
                            elif thaido_tts_lop.nhanxet_ythucxau:
                                csdt_0031 = nhanxet_ythucxau
                            else:
                                csdt_0031 = '*'

                            infor_danhgia_nangluc['csdt_0031'] = Listing(csdt_0031)
                    table_daotao.append(infor_danhgia_nangluc)
            context['tbl_baihoc'] = table_daotao

            table_diem_n5 = []
            for i in volong.danhsach_thuctapsinh_volong.diem_n5:
                diem = {}
                if i.thang_danhgia_n5:
                    diem['csdt_0041'] = date_time_in_jp(i.thang_danhgia_n5.day,
                                                        i.thang_danhgia_n5.month,
                                                        i.thang_danhgia_n5.year)
                else:
                    diem['csdt_0041'] = u'年　月　日'
                diem['csdt_0032'] = i.tuvung_lop if i.tuvung_lop else ''
                diem['csdt_0033'] = i.doc_nguphap_lop if i.doc_nguphap_lop else ''
                diem['csdt_0034'] = i.nghe_hieu_lop if i.nghe_hieu_lop else ''
                diem['csdt_0035'] = i.hoi_thoai_lop if i.hoi_thoai_lop else ''
                diem['csdt_0036'] = i.tong_lop if i.tong_lop else ''
                diem['csdt_0039'] = i.danhgia_n5 if i.danhgia_n5 else ''
                if i.tong_lop or i.thang_danhgia_n5:
                    if i.do_lop == True:
                        diem['csdt_0037'] = u'✔'
                    else:
                        diem['csdt_0038'] = u'✔'
                else:
                    diem['csdt_0037'] = ''
                    diem['csdt_0038'] = ''
                table_diem_n5.append(diem)
            context['tbl_n5'] = table_diem_n5

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

    kha = fields.Integer(string="Khá giỏi", default=70)
    trungbinh = fields.Integer(string="Trung bình", default=50)
    yeu = fields.Integer(string="Yếu", default=35)

    phantram_kha = fields.Float(string='Phần trăm khá', compute='baocao_nangluc_button')
    phantram_tb = fields.Float(string='Phần trăm trung bình', compute='baocao_nangluc_button')
    phantram_yeu = fields.Float(string='Phần trăm yếu', compute='baocao_nangluc_button')
    phantram_cabiet = fields.Float(string='Phần trăm cá biệt', compute='baocao_nangluc_button')
    phantram_khongcodiem = fields.Float(string="Phần trăm không có điểm", compute='baocao_nangluc_button')
    songuoi_kha = fields.Integer(string='Số người khá', compute='baocao_nangluc_button')
    songuoi_tb = fields.Integer(string='Số người trung bình', compute='baocao_nangluc_button')
    songuoi_yeu = fields.Integer(string='Số người yếu', compute='baocao_nangluc_button')
    songuoi_cabiet = fields.Integer(string='Số người cá biệt', compute='baocao_nangluc_button')
    songuoi_chuacodiem = fields.Integer(string='Số người chưa có điểm', compute='baocao_nangluc_button')
    tong_songuoi = fields.Integer(string='Tổng số người', compute='baocao_nangluc_button')
    songuoi_codiem = fields.Integer(string='Tổng số người có điểm', compute='baocao_nangluc_button')

    tang_giam_kha = fields.Float()
    tang_giam_kha_1 = fields.Integer()
    tang_giam_tb = fields.Float()
    tang_giam_tb_1 = fields.Integer()
    tang_giam_yeu = fields.Float()
    tang_giam_yeu_1 = fields.Integer()
    tang_giam_cabiet = fields.Float()
    tang_giam_cabiet_1 = fields.Integer()
    tang_giam_kocodiem = fields.Float()
    tang_giam_kocodiem_1 = fields.Integer()

    @api.multi
    def baocao_nangluc_button(self):
        for rec in self:
            if rec.tenlop_nhapdiem and rec.volong_nhapdiem:
                for danhsach_tts in rec.danhsach_thuctapsinh_volong:
                    danhsach_tts.write({'tenlop_danhsach': rec.tenlop_nhapdiem.id})
            elif rec.tenlop_nhapdiem and rec.volong_nhapdiem == False:
                for danhsach in rec.danhsach_thuctapsinh:
                    danhsach.write(
                        {'tenlop_danhsach': rec.tenlop_nhapdiem.id})

            rec.tong_songuoi = len(rec.danhsach_thuctapsinh)

            kha = 0
            trungbinh = 0
            yeu = 0
            cabiet = 0
            chuaco_diem = 0
            for ie in rec.danhsach_thuctapsinh:
                if ie.kha_b == True:
                    kha += 1
                if ie.trungbinh_b == True:
                    trungbinh += 1
                if ie.yeukem_b == True:
                    yeu += 1
                if ie.ca_biet == True:
                    cabiet += 1
                if ie.chua_codiem == True:
                    chuaco_diem += 1
            if cabiet:
                rec.phantram_cabiet = round((float(cabiet) / float(len(rec.danhsach_thuctapsinh)) * 100), 1)
            if chuaco_diem:
                rec.phantram_khongcodiem = round((float(chuaco_diem) / float(len(rec.danhsach_thuctapsinh)) * 100), 1)
            if kha:
                rec.phantram_kha = round((float(kha) / float(len(rec.danhsach_thuctapsinh)) * 100), 1)
            if trungbinh:
                rec.phantram_tb = round((float(trungbinh) / float(len(rec.danhsach_thuctapsinh)) * 100), 1)
            if yeu:
                rec.phantram_yeu = round((float(yeu) / float(len(rec.danhsach_thuctapsinh)) * 100), 1)

            rec.songuoi_kha = kha
            rec.songuoi_tb = trungbinh
            rec.songuoi_yeu = yeu
            rec.songuoi_cabiet = cabiet
            rec.songuoi_chuacodiem = chuaco_diem


class BaihocDanhgia(models.Model):
    _name = 'baihoc'
    _rec_name = 'baihocdanhgia'

    baihocdanhgia = fields.Char('Bài học')


class Danhgia(models.Model):
    _name = 'danhgia'
    _rec_name = 'name'

    name = fields.Char('Đánh giá')


class Baocaolop(models.Model):
    _name = 'baocao.lop'
    _rec_name = 'ten_trungtam'

    ten_trungtam = fields.Many2one(comodel_name='trungtam.daotao', string='Trung tâm đào tạo')
    ngay_star = fields.Date(string='Ngày bắt đầu', default=fields.Date.today)
    ngay_end = fields.Date(string='kết thúc', default=fields.Date.today)
    lop = fields.Many2one(comodel_name='taolop.taolop', string="Tên lớp")
    danhsach_thuctapsinh_baocao = fields.Many2many(comodel_name='nhapdiem.nhapdiem', string='Danh sách thực tập sinh',
                                                   compute='onchange_ten_trungtam', default_order="ten_trungtam asc")

    @api.multi
    @api.onchange('ten_trungtam', 'ngay_star', 'ngay_end', 'lop')
    def onchange_ten_trungtam(self):
        if self.ngay_star > self.ngay_end:
            raise except_orm('Ngày bắt đầu phải nhỏ hơn hoặc bằng ngày kết thúc')
        else:
            if self.ngay_star:
                batdau = self.ngay_star - timedelta(days=7)
            else:
                batdau = datetime.today()

            if self.ten_trungtam:
                if self.lop:
                    related_ids = []
                    # Shearch danh sach thuc tap sinh
                    thuctapsinh_baocao = self.env['nhapdiem.nhapdiem'].search(
                        [('trungtam_nhapdiem', '=', self.ten_trungtam.id), ('tenlop_nhapdiem', '=', self.lop.id),
                         ('tuan', '>=', batdau), ('tuan', '<=', self.ngay_end),
                         ('tuan_nhapdiem', 'in', ['1', '2', '3', '4'])], order="trungtam_nhapdiem asc")
                    # Append danh sach thuc tap sinh
                    for baocao in thuctapsinh_baocao:
                        related_ids.append(baocao.id)
                    # Gan vao danhsach_thuctapsinh_baocao
                    self.danhsach_thuctapsinh_baocao = self.env['nhapdiem.nhapdiem'].search([('id', 'in', related_ids)],
                                                                                            order="trungtam_nhapdiem asc,tenlop_nhapdiem asc,thang_nhapdiem asc,tuan_nhapdiem asc")

    @api.multi
    def depends_lop_diem_button(self):
        for rec in self:
            if rec.ngay_star:
                batdau = rec.ngay_star - timedelta(days=7)
            else:
                batdau = datetime.today()

            for diem_tuan in rec.danhsach_thuctapsinh_baocao:
                lop_diem = rec.env['nhapdiem.nhapdiem'].search(
                    [('tuan', '<=', rec.ngay_end), ('tuan', '>=', batdau),
                     ('tenlop_nhapdiem', '=', diem_tuan.tenlop_nhapdiem.id)])
                sosanh_tb = 0
                trungbinh = 0
                sosanh_yeu = 0
                yeu = 0
                sosanh_kha = 0
                kha_kha = 0

                sosanh_tb_1 = 0
                trungbinh_1 = 0
                sosanh_yeu_1 = 0
                yeu_1 = 0
                sosanh_kha_1 = 0
                kha_kha_1 = 0

                cabiet = 0
                sosanh_cabiet = 0
                kocodiem = 0
                sosanh_kocodiem = 0

                cabiet_1 = 0
                sosanh_cabiet_1 = 0
                kocodiem_1 = 0
                sosanh_kocodiem_1 = 0
                for item in lop_diem:
                    # so sánh phần trăm khá
                    sosanh_kha = item.phantram_kha - kha_kha
                    item.tang_giam_kha = sosanh_kha
                    kha_kha = item.phantram_kha

                    # so sánh phần trăm trung bình
                    sosanh_tb = item.phantram_tb - trungbinh
                    item.tang_giam_tb = sosanh_tb
                    trungbinh = item.phantram_tb

                    # so sánh phần trăm yếu
                    sosanh_yeu = item.phantram_yeu - yeu
                    item.tang_giam_yeu = sosanh_yeu
                    yeu = item.phantram_yeu

                    # so sánh phần trăm cá biệt
                    sosanh_cabiet = item.phantram_cabiet - cabiet
                    item.tang_giam_cabiet = sosanh_cabiet
                    cabiet = item.phantram_cabiet

                    # so sánh phần trăm không có điểm
                    sosanh_kocodiem = item.phantram_khongcodiem - kocodiem
                    item.tang_giam_kocodiem = sosanh_kocodiem
                    kocodiem = item.phantram_khongcodiem

                    # so sánh số người khá
                    sosanh_kha_1 = item.songuoi_kha - kha_kha_1
                    item.tang_giam_kha_1 = sosanh_kha_1
                    kha_kha_1 = item.songuoi_kha

                    # so sánh số người trung bình
                    sosanh_tb_1 = item.songuoi_tb - trungbinh_1
                    item.tang_giam_tb_1 = sosanh_tb_1
                    trungbinh_1 = item.songuoi_tb

                    # so sánh số người yếu
                    sosanh_yeu_1 = item.songuoi_yeu - yeu_1
                    item.tang_giam_yeu_1 = sosanh_yeu_1
                    yeu_1 = item.songuoi_yeu

                    # so sánh số người cá biệt
                    sosanh_cabiet_1 = item.songuoi_cabiet - cabiet_1
                    item.tang_giam_cabiet_1 = sosanh_cabiet_1
                    cabiet_1 = item.songuoi_cabiet

                    # so sánh số người không có điểm
                    sosanh_kocodiem_1 = item.songuoi_chuacodiem - kocodiem_1
                    item.tang_giam_kocodiem_1 = sosanh_kocodiem_1
                    kocodiem_1 = item.songuoi_chuacodiem
        return {
            'type': 'ir.actions.act_url',
            'url': '/baocao/diem/%s' % (self.id),
            'target': 'self', }

    def baocao_theotuan_diem(self, document):
        if document.ten_trungtam:
            tong_tts = document.env['lop.thuctapsinh'].search(
                [('trungtam_lop', '=', document.ten_trungtam.id), ('trangthai', '=', '1')])
            if document.lop:
                tong_tts = document.env['lop.thuctapsinh'].search(
                    [('trungtam_lop', '=', document.ten_trungtam.id), ('tenlop_thuctapsinh', '=', document.lop.id),
                     ('trangthai', '=', '1')])
        else:
            tong_tts = document.env['lop.thuctapsinh'].search([('trangthai', '=', '1')])

        docs = self.env['intern.document'].search([('name', '=', "baocaotuan")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].attachment, "base64"))
            docdata = DocxTemplate(stream)
            context = {}
            context['tt_0001'] = len(tong_tts)
            context['tt_0002'] = str(document.ngay_star.day) + '/' + str(document.ngay_star.month) + '/' + \
                                 str(document.ngay_star.year)
            context['tt_0003'] = str(document.ngay_end.day) + '/' + str(document.ngay_end.month) + '/' + \
                                 str(document.ngay_end.year)
            if document.ngay_star:
                batdau = document.ngay_star - timedelta(days=7)
            else:
                batdau = datetime.today()
            table_danhsach_tuan = []
            item = 0
            for diem_tuan in document.danhsach_thuctapsinh_baocao:
                infor = {}
                if diem_tuan.tuan < document.ngay_star and diem_tuan.tuan >= batdau:
                    if item > 0:
                        if diem_tuan.tang_giam_kha > 0:
                            infor['tt_0009'] = u'↑(' + str(diem_tuan.tang_giam_kha) + u'%)'
                        elif diem_tuan.tang_giam_kha < 0:
                            infor['tt_0009'] = u'↓(' + str(abs(diem_tuan.tang_giam_kha)) + u'%)'
                        else:
                            infor['tt_0009'] = u'(' + str(abs(diem_tuan.tang_giam_kha)) + u'%)'

                        if diem_tuan.tang_giam_tb > 0:
                            infor['tt_0012'] = u'↑(' + str(diem_tuan.tang_giam_tb) + u'%)'
                        elif diem_tuan.tang_giam_tb < 0:
                            infor['tt_0012'] = u'↓(' + str(abs(diem_tuan.tang_giam_tb)) + u'%)'
                        else:
                            infor['tt_0012'] = u'(' + str(abs(diem_tuan.tang_giam_tb)) + u'%)'

                        if diem_tuan.tang_giam_yeu > 0:
                            infor['tt_0015'] = u'↑(' + str(diem_tuan.tang_giam_yeu) + u'%)'
                        elif diem_tuan.tang_giam_yeu < 0:
                            infor['tt_0015'] = u'↓(' + str(abs(diem_tuan.tang_giam_yeu)) + u'%)'
                        else:
                            infor['tt_0015'] = u'(' + str(abs(diem_tuan.tang_giam_yeu)) + u'%)'

                        if diem_tuan.tang_giam_cabiet > 0:
                            infor['tt_0018'] = u'↑(' + str(diem_tuan.tang_giam_cabiet) + u'%)'
                        elif diem_tuan.tang_giam_cabiet < 0:
                            infor['tt_0018'] = u'↓(' + str(abs(diem_tuan.tang_giam_cabiet)) + u'%)'
                        else:
                            infor['tt_0018'] = u'(' + str(abs(diem_tuan.tang_giam_cabiet)) + u'%)'

                        if diem_tuan.tang_giam_kocodiem > 0:
                            infor['tt_0021'] = u'↑(' + str(diem_tuan.tang_giam_kocodiem) + u'%)'
                        elif diem_tuan.tang_giam_kocodiem < 0:
                            infor['tt_0021'] = u'↓(' + str(abs(diem_tuan.tang_giam_kocodiem)) + u'%)'
                        else:
                            infor['tt_0021'] = u'(' + str(abs(diem_tuan.tang_giam_kocodiem)) + u'%)'

                        infor['tt_0004'] = diem_tuan.trungtam_nhapdiem.name_trungtam
                        infor['tt_0042'] = diem_tuan.tuan_nhapdiem
                        infor['tt_0041'] = str(diem_tuan.tuan.day) + '/' + str(diem_tuan.tuan.month) + '/' + \
                                           str(diem_tuan.tuan.year)
                        infor['tt_0005'] = diem_tuan.tenlop_nhapdiem.tenlop_taolop
                        infor['tt_0006'] = diem_tuan.giaovienchunhiem_nhapdiem.partner_id.name
                        infor['tt_0007'] = diem_tuan.songuoi_kha
                        infor['tt_0010'] = diem_tuan.songuoi_tb
                        infor['tt_0013'] = diem_tuan.songuoi_yeu
                        infor['tt_0016'] = diem_tuan.songuoi_cabiet
                        infor['tt_0019'] = diem_tuan.songuoi_chuacodiem
                        infor['tt_0022'] = diem_tuan.tong_songuoi
                        infor['tt_0008'] = str(diem_tuan.phantram_kha) + u'%'
                        infor['tt_0011'] = str(diem_tuan.phantram_tb) + u'%'
                        infor['tt_0014'] = str(diem_tuan.phantram_yeu) + u'%'
                        infor['tt_0017'] = str(diem_tuan.phantram_cabiet) + u'%'
                        infor['tt_0020'] = str(diem_tuan.phantram_khongcodiem) + u'%'
                        table_danhsach_tuan.append(infor)
                else:
                    if diem_tuan.tang_giam_kha > 0:
                        infor['tt_0009'] = u'↑(' + str(diem_tuan.tang_giam_kha) + u'%)'
                    elif diem_tuan.tang_giam_kha < 0:
                        infor['tt_0009'] = u'↓(' + str(abs(diem_tuan.tang_giam_kha)) + u'%)'
                    else:
                        infor['tt_0009'] = u'(' + str(abs(diem_tuan.tang_giam_kha)) + u'%)'

                    if diem_tuan.tang_giam_tb > 0:
                        infor['tt_0012'] = u'↑(' + str(diem_tuan.tang_giam_tb) + u'%)'
                    elif diem_tuan.tang_giam_tb < 0:
                        infor['tt_0012'] = u'↓(' + str(abs(diem_tuan.tang_giam_tb)) + u'%)'
                    else:
                        infor['tt_0012'] = u'(' + str(abs(diem_tuan.tang_giam_tb)) + u'%)'

                    if diem_tuan.tang_giam_yeu > 0:
                        infor['tt_0015'] = u'↑(' + str(diem_tuan.tang_giam_yeu) + u'%)'
                    elif diem_tuan.tang_giam_yeu < 0:
                        infor['tt_0015'] = u'↓(' + str(abs(diem_tuan.tang_giam_yeu)) + u'%)'
                    else:
                        infor['tt_0015'] = u'(' + str(abs(diem_tuan.tang_giam_yeu)) + u'%)'

                    if diem_tuan.tang_giam_cabiet > 0:
                        infor['tt_0018'] = u'↑(' + str(diem_tuan.tang_giam_cabiet) + u'%)'
                    elif diem_tuan.tang_giam_cabiet < 0:
                        infor['tt_0018'] = u'↓(' + str(abs(diem_tuan.tang_giam_cabiet)) + u'%)'
                    else:
                        infor['tt_0018'] = u'(' + str(abs(diem_tuan.tang_giam_cabiet)) + u'%)'

                    if diem_tuan.tang_giam_kocodiem > 0:
                        infor['tt_0021'] = u'↑(' + str(diem_tuan.tang_giam_kocodiem) + u'%)'
                    elif diem_tuan.tang_giam_kocodiem < 0:
                        infor['tt_0021'] = u'↓(' + str(abs(diem_tuan.tang_giam_kocodiem)) + u'%)'
                    else:
                        infor['tt_0021'] = u'(' + str(abs(diem_tuan.tang_giam_kocodiem)) + u'%)'

                    infor['tt_0004'] = diem_tuan.trungtam_nhapdiem.name_trungtam
                    infor['tt_0042'] = diem_tuan.tuan_nhapdiem
                    infor['tt_0041'] = str(diem_tuan.tuan.day) + '/' + str(diem_tuan.tuan.month) + '/' + \
                                       str(diem_tuan.tuan.year)
                    infor['tt_0005'] = diem_tuan.tenlop_nhapdiem.tenlop_taolop
                    infor['tt_0006'] = diem_tuan.giaovienchunhiem_nhapdiem.partner_id.name
                    infor['tt_0007'] = diem_tuan.songuoi_kha
                    infor['tt_0010'] = diem_tuan.songuoi_tb
                    infor['tt_0013'] = diem_tuan.songuoi_yeu
                    infor['tt_0016'] = diem_tuan.songuoi_cabiet
                    infor['tt_0019'] = diem_tuan.songuoi_chuacodiem
                    infor['tt_0022'] = diem_tuan.tong_songuoi
                    infor['tt_0008'] = str(diem_tuan.phantram_kha) + u'%'
                    infor['tt_0011'] = str(diem_tuan.phantram_tb) + u'%'
                    infor['tt_0014'] = str(diem_tuan.phantram_yeu) + u'%'
                    infor['tt_0017'] = str(diem_tuan.phantram_cabiet) + u'%'
                    infor['tt_0020'] = str(diem_tuan.phantram_khongcodiem) + u'%'
                    table_danhsach_tuan.append(infor)
                item += 1
            context['danhsachlop'] = table_danhsach_tuan

            document.unlink()

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.seek(0)
            return tempFile
        return None


class trungtamdiem(models.Model):
    _name = 'baocao.trungtam'
    _rec_name = 'ten_trungtam_1'

    ten_trungtam_1 = fields.Many2one(comodel_name='trungtam.daotao', string='Trung tâm đào tạo')
    ngay_star_1 = fields.Date(string='Ngày bắt đầu', default=fields.Date.today())
    ngay_end_1 = fields.Date(string='Ngày kết thúc', default=fields.Date.today())
    danhsach_trungtam_baocao = fields.Many2many(comodel_name='trungtam.diem', string='Danh sách trung tâm',
                                                compute='onchange_ten_trungtam_trungtam')

    @api.multi
    @api.onchange('ten_trungtam_1', 'ngay_star_1', 'ngay_end_1')
    def onchange_ten_trungtam_trungtam(self):
        if self.ngay_star_1 > self.ngay_end_1:
            raise except_orm('Ngày bắt đầu phải nhỏ hơn hoặc bằng ngày kết thúc')
        else:
            if self.ngay_star_1:
                batdau = self.ngay_star_1 - timedelta(days=7)
            else:
                batdau = datetime.today()
            related_ids = []
            # Shearch danh sach thuc tap sinh
            trungtam_baocao = self.env['trungtam.diem'].search([('ngay_tao', '>=', batdau),
                                                                ('ngay_tao', '<=', self.ngay_end_1)])
            for baocao in trungtam_baocao:
                related_ids.append(baocao.id)
            # Gan vao danhsach_trungtam_baocao
            self.danhsach_trungtam_baocao = self.env['trungtam.diem'].search([('id', 'in', related_ids)],
                                                                             order="ten_trungtam asc,thang asc,tuan_trungtam asc")
            if self.ten_trungtam_1:
                related_ids = []
                # Shearch danh sach thuc tap sinh
                trungtam_baocao = self.env['trungtam.diem'].search(
                    [('ngay_tao', '>=', batdau), ('ngay_tao', '<=', self.ngay_end_1),
                     ('ten_trungtam', '=', self.ten_trungtam_1.id)],
                    order="ten_trungtam asc,thang asc,tuan_trungtam asc")
                # Append danh sach thuc tap sinh
                for baocao in trungtam_baocao:
                    related_ids.append(baocao.id)
                # Gan vao danhsach_trungtam_baocao
                self.danhsach_trungtam_baocao = self.env['trungtam.diem'].search([('id', 'in', related_ids)],
                                                                                 order="ten_trungtam asc,thang asc,tuan_trungtam asc")

    @api.multi
    def depends_trungtam_diem_button(self):
        for rec in self:
            if rec.ngay_star_1:
                batdau = rec.ngay_star_1 - timedelta(days=7)
            else:
                batdau = datetime.today()
            for diem_tuan in rec.danhsach_trungtam_baocao:
                lop_diem = rec.env['trungtam.diem'].search(
                    [('ngay_tao', '<=', rec.ngay_end_1),
                     ('ngay_tao', '>=', batdau),
                     ('ten_trungtam', '=', diem_tuan.ten_trungtam.id)],
                    order="ten_trungtam asc,thang asc,tuan_trungtam asc")
                sosanh_tb = 0
                trungbinh = 0
                sosanh_yeu = 0
                yeu = 0
                sosanh_kha = 0
                kha_kha = 0

                sosanh_tb_1 = 0
                trungbinh_1 = 0
                sosanh_yeu_1 = 0
                yeu_1 = 0
                sosanh_kha_1 = 0
                kha_kha_1 = 0

                cabiet = 0
                sosanh_cabiet = 0
                kocodiem = 0
                sosanh_kocodiem = 0

                cabiet_1 = 0
                sosanh_cabiet_1 = 0
                kocodiem_1 = 0
                sosanh_kocodiem_1 = 0
                for item in lop_diem:
                    # so sánh phần trăm khá
                    sosanh_kha = item.phantram_kha - kha_kha
                    item.tang_giam_kha = sosanh_kha
                    kha_kha = item.phantram_kha

                    # so sánh phần trăm trung bình
                    sosanh_tb = item.phantram_tb - trungbinh
                    item.tang_giam_tb = sosanh_tb
                    trungbinh = item.phantram_tb

                    # so sánh phần trăm yếu
                    sosanh_yeu = item.phantram_yeu - yeu
                    item.tang_giam_yeu = sosanh_yeu
                    yeu = item.phantram_yeu

                    # so sánh phần trăm cá biệt
                    sosanh_cabiet = item.phantram_cabiet - cabiet
                    item.tang_giam_cabiet = sosanh_cabiet
                    cabiet = item.phantram_cabiet

                    # so sánh phần trăm không có điểm
                    sosanh_kocodiem = item.phantram_khongcodiem - kocodiem
                    item.tang_giam_kocodiem = sosanh_kocodiem
                    kocodiem = item.phantram_khongcodiem

                    # so sánh số người khá
                    sosanh_kha_1 = item.songuoi_kha - kha_kha_1
                    item.tang_giam_kha_1 = sosanh_kha_1
                    kha_kha_1 = item.songuoi_kha

                    # so sánh số người trung bình
                    sosanh_tb_1 = item.songuoi_tb - trungbinh_1
                    item.tang_giam_tb_1 = sosanh_tb_1
                    trungbinh_1 = item.songuoi_tb

                    # so sánh số người yếu
                    sosanh_yeu_1 = item.songuoi_yeu - yeu_1
                    item.tang_giam_yeu_1 = sosanh_yeu_1
                    yeu_1 = item.songuoi_yeu

                    # so sánh số người cá biệt
                    sosanh_cabiet_1 = item.songuoi_cabiet - cabiet_1
                    item.tang_giam_cabiet_1 = sosanh_cabiet_1
                    cabiet_1 = item.songuoi_cabiet

                    # so sánh số người không có điểm
                    sosanh_kocodiem_1 = item.songuoi_chuacodiem - kocodiem_1
                    item.tang_giam_kocodiem_1 = sosanh_kocodiem_1
                    kocodiem_1 = item.songuoi_chuacodiem
        return {
            'type': 'ir.actions.act_url',
            'url': '/diem/diem_trungtam/%s' % (self.id),
            'target': 'self', }

    def baocao_theotuan_trungtam_diem(self, document):
        if document.ten_trungtam_1:
            tong_tts = document.env['lop.thuctapsinh'].search(
                [('trungtam_lop', '=', document.ten_trungtam_1.id), ('trangthai', '=', '1')])
        else:
            tong_tts = document.env['lop.thuctapsinh'].search([('trangthai', '=', '1')])
        docs = self.env['intern.document'].search([('name', '=', "baocaotuan1")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].attachment, "base64"))
            docdata = DocxTemplate(stream)
            context = {}

            context['tt_0001'] = len(tong_tts)
            context['tt_0002'] = str(document.ngay_star_1.day) + '/' + str(document.ngay_star_1.month) + '/' + \
                                 str(document.ngay_star_1.year)
            context['tt_0003'] = str(document.ngay_end_1.day) + '/' + str(document.ngay_end_1.month) + '/' + \
                                 str(document.ngay_end_1.year)
            table_danhsach_trungtam = []

            item = 0
            if document.ngay_star_1:
                batdau = document.ngay_star_1 - timedelta(days=7)
            else:
                batdau = datetime.today()
            for diem_tuan in document.danhsach_trungtam_baocao:
                infor = {}
                if diem_tuan.ngay_tao < document.ngay_star_1 and diem_tuan.ngay_tao >= batdau:
                    if item > 0:
                        if diem_tuan.tang_giam_kha > 0:
                            infor['tt_0009'] = u'↑(' + str(diem_tuan.tang_giam_kha) + u'%)'
                        elif diem_tuan.tang_giam_kha < 0:
                            infor['tt_0009'] = u'↓(' + str(abs(diem_tuan.tang_giam_kha)) + u'%)'
                        else:
                            infor['tt_0009'] = u' (' + str(abs(diem_tuan.tang_giam_kha)) + u'%)'

                        if diem_tuan.tang_giam_tb > 0:
                            infor['tt_0012'] = u'↑(' + str(diem_tuan.tang_giam_tb) + u'%)'
                        elif diem_tuan.tang_giam_tb < 0:
                            infor['tt_0012'] = u'↓(' + str(abs(diem_tuan.tang_giam_tb)) + u'%)'
                        else:
                            infor['tt_0012'] = u'(' + str(abs(diem_tuan.tang_giam_tb)) + u'%)'

                        if diem_tuan.tang_giam_yeu > 0:
                            infor['tt_0015'] = u'↑(' + str(diem_tuan.tang_giam_yeu) + u'%)'
                        elif diem_tuan.tang_giam_yeu < 0:
                            infor['tt_0015'] = u'↓(' + str(abs(diem_tuan.tang_giam_yeu)) + u'%)'
                        else:
                            infor['tt_0015'] = u'(' + str(abs(diem_tuan.tang_giam_yeu)) + u'%)'

                        if diem_tuan.tang_giam_cabiet > 0:
                            infor['tt_0018'] = u'↑(' + str(diem_tuan.tang_giam_cabiet) + u'%)'
                        elif diem_tuan.tang_giam_cabiet < 0:
                            infor['tt_0018'] = u'↓(' + str(abs(diem_tuan.tang_giam_cabiet)) + u'%)'
                        else:
                            infor['tt_0018'] = u' (' + str(abs(diem_tuan.tang_giam_cabiet)) + u'%)'

                        if diem_tuan.tang_giam_kocodiem > 0:
                            infor['tt_0021'] = u'↑(' + str(diem_tuan.tang_giam_kocodiem) + u'%)'
                        elif diem_tuan.tang_giam_kocodiem < 0:
                            infor['tt_0021'] = u'↓(' + str(abs(diem_tuan.tang_giam_kocodiem)) + u'%)'
                        else:
                            infor['tt_0021'] = u' (' + str(abs(diem_tuan.tang_giam_kocodiem)) + u'%)'

                        infor['tt_0004'] = diem_tuan.ten_trungtam.name_trungtam
                        infor['tt_0006'] = str(diem_tuan.ngay_tao.day) + '/' + str(diem_tuan.ngay_tao.month) + '/' + \
                                           str(diem_tuan.ngay_tao.year)
                        infor['tt_0042'] = diem_tuan.tuan_trungtam
                        infor['tt_0007'] = diem_tuan.songuoi_kha
                        infor['tt_0010'] = diem_tuan.songuoi_tb
                        infor['tt_0013'] = diem_tuan.songuoi_yeu
                        infor['tt_0016'] = diem_tuan.songuoi_cabiet
                        infor['tt_0019'] = diem_tuan.songuoi_chuacodiem
                        infor['tt_0022'] = diem_tuan.tong_songuoi
                        infor['tt_0008'] = str(diem_tuan.phantram_kha) + u'%'
                        infor['tt_0011'] = str(diem_tuan.phantram_tb) + u'%'
                        infor['tt_0014'] = str(diem_tuan.phantram_yeu) + u'%'
                        infor['tt_0017'] = str(diem_tuan.phantram_cabiet) + u'%'
                        infor['tt_0020'] = str(diem_tuan.phantram_khongcodiem) + u'%'
                        table_danhsach_trungtam.append(infor)
                else:
                    if diem_tuan.tang_giam_kha > 0:
                        infor['tt_0009'] = u'↑(' + str(diem_tuan.tang_giam_kha) + u'%)'
                    elif diem_tuan.tang_giam_kha < 0:
                        infor['tt_0009'] = u'↓(' + str(abs(diem_tuan.tang_giam_kha)) + u'%)'
                    else:
                        infor['tt_0009'] = u' (' + str(abs(diem_tuan.tang_giam_kha)) + u'%)'

                    if diem_tuan.tang_giam_tb > 0:
                        infor['tt_0012'] = u'↑(' + str(diem_tuan.tang_giam_tb) + u'%)'
                    elif diem_tuan.tang_giam_tb < 0:
                        infor['tt_0012'] = u'↓(' + str(abs(diem_tuan.tang_giam_tb)) + u'%)'
                    else:
                        infor['tt_0012'] = u'(' + str(abs(diem_tuan.tang_giam_tb)) + u'%)'

                    if diem_tuan.tang_giam_yeu > 0:
                        infor['tt_0015'] = u'↑(' + str(diem_tuan.tang_giam_yeu) + u'%)'
                    elif diem_tuan.tang_giam_yeu < 0:
                        infor['tt_0015'] = u'↓(' + str(abs(diem_tuan.tang_giam_yeu)) + u'%)'
                    else:
                        infor['tt_0015'] = u'(' + str(abs(diem_tuan.tang_giam_yeu)) + u'%)'

                    if diem_tuan.tang_giam_cabiet > 0:
                        infor['tt_0018'] = u'↑(' + str(diem_tuan.tang_giam_cabiet) + u'%)'
                    elif diem_tuan.tang_giam_cabiet < 0:
                        infor['tt_0018'] = u'↓(' + str(abs(diem_tuan.tang_giam_cabiet)) + u'%)'
                    else:
                        infor['tt_0018'] = u' (' + str(abs(diem_tuan.tang_giam_cabiet)) + u'%)'

                    if diem_tuan.tang_giam_kocodiem > 0:
                        infor['tt_0021'] = u'↑(' + str(diem_tuan.tang_giam_kocodiem) + u'%)'
                    elif diem_tuan.tang_giam_kocodiem < 0:
                        infor['tt_0021'] = u'↓(' + str(abs(diem_tuan.tang_giam_kocodiem)) + u'%)'
                    else:
                        infor['tt_0021'] = u' (' + str(abs(diem_tuan.tang_giam_kocodiem)) + u'%)'

                    infor['tt_0004'] = diem_tuan.ten_trungtam.name_trungtam
                    infor['tt_0006'] = str(diem_tuan.ngay_tao.day) + '/' + str(diem_tuan.ngay_tao.month) + '/' + \
                                       str(diem_tuan.ngay_tao.year)
                    infor['tt_0042'] = diem_tuan.tuan_trungtam
                    infor['tt_0007'] = diem_tuan.songuoi_kha
                    infor['tt_0010'] = diem_tuan.songuoi_tb
                    infor['tt_0013'] = diem_tuan.songuoi_yeu
                    infor['tt_0016'] = diem_tuan.songuoi_cabiet
                    infor['tt_0019'] = diem_tuan.songuoi_chuacodiem
                    infor['tt_0022'] = diem_tuan.tong_songuoi
                    infor['tt_0008'] = str(diem_tuan.phantram_kha) + u'%'
                    infor['tt_0011'] = str(diem_tuan.phantram_tb) + u'%'
                    infor['tt_0014'] = str(diem_tuan.phantram_yeu) + u'%'
                    infor['tt_0017'] = str(diem_tuan.phantram_cabiet) + u'%'
                    infor['tt_0020'] = str(diem_tuan.phantram_khongcodiem) + u'%'
                    table_danhsach_trungtam.append(infor)
                item += 1
            context['danhsachlop'] = table_danhsach_trungtam

            document.unlink()
            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.seek(0)
            # tempFile.flush()
            # tempFile.close()
            return tempFile
        return None


class Trungtamdiem(models.Model):
    _name = 'trungtam.diem'
    _rec_name = 'ten_trungtam'

    ten_trungtam = fields.Many2one(comodel_name='trungtam.daotao', string='Trung tâm đào tạo')
    tuan_trungtam = fields.Selection(string="Tuần", selection=[('1', 'Tuần 1'), ('2', 'Tuần 2'), ('3', 'Tuần 3'),
                                                               ('4', 'Tuần 4'), ('5', 'Vỡ lòng')], required=True)
    thang = fields.Selection(string="Tháng", selection=[('1', 'Tháng 1'), ('2', 'Tháng 2'), ('3', 'Tháng 3'),
                                                        ('4', 'Tháng 4'), ('5', 'Tháng 5'), ('6', 'Tháng 6'),
                                                        ('7', 'Tháng 7'), ('8', 'Tháng 8'), ('9', 'Tháng 9'),
                                                        ('10', 'Tháng 10'), ('11', 'Tháng 11'),
                                                        ('12', 'Tháng 12'), ('13', 'Vỡ lòng')], required=True)
    ngay_tao = fields.Date(string="Ngày tạo", required=True, default=fields.Date.today())
    nam_trungtam = fields.Char('Năm')
    danhsach_trungtam_baocao = fields.Many2many(comodel_name='danhsach.nhapdiem', string='Danh sách thực tập sinh',
                                                compute='onchange_ten_trungtam_tuan')

    @api.multi
    @api.onchange('ten_trungtam', 'tuan_trungtam', 'thang')
    def onchange_ten_trungtam_tuan(self):
        related_ids = []
        if self.ten_trungtam:
            # Shearch danh sach thuc tap sinh
            trungtam_baocao = self.env['danhsach.nhapdiem'].search(
                [('trungtam_danhsach', '=', self.ten_trungtam.id),
                 ('tuan_danhsach', '=', self.tuan_trungtam),
                 ('nam_danhgia', '=', self.nam_trungtam),
                 ('thang_danhgia', '=', self.thang)])
            # Append danh sach thuc tap sinh
            for baocao in trungtam_baocao:
                related_ids.append(baocao.id)

            # Gan vao danhsach_trungtam_baocao
            self.danhsach_trungtam_baocao = self.env['danhsach.nhapdiem'].search([('id', 'in', related_ids)])

    phantram_kha = fields.Float(string='Phần trăm khá', compute='_depends_theotuan_diem_trungtam')
    phantram_tb = fields.Float(string='Phần trăm trung bình', compute='_depends_theotuan_diem_trungtam')
    phantram_yeu = fields.Float(string='Phần trăm yếu', compute='_depends_theotuan_diem_trungtam')
    phantram_cabiet = fields.Float(string='Phần trăm cá biệt', compute='_depends_theotuan_diem_trungtam')
    phantram_khongcodiem = fields.Float(string="Không có điểm", compute='_depends_theotuan_diem_trungtam')
    songuoi_kha = fields.Integer(string='Số người khá', compute='_depends_theotuan_diem_trungtam')
    songuoi_tb = fields.Integer(string='Số người trung bình', compute='_depends_theotuan_diem_trungtam')
    songuoi_yeu = fields.Integer(string='Số người yếu', compute='_depends_theotuan_diem_trungtam')
    songuoi_cabiet = fields.Integer(string='Số người cá biệt', compute='_depends_theotuan_diem_trungtam')
    songuoi_chuacodiem = fields.Integer(string='Số người chưa có điểm', compute='_depends_theotuan_diem_trungtam')
    tong_songuoi = fields.Integer(string='Tổng số người', compute='_depends_theotuan_diem_trungtam')
    songuoi_codiem = fields.Integer(string='Tổng số người có điểm', compute='_depends_theotuan_diem_trungtam')

    tang_giam_kha = fields.Float()
    tang_giam_kha_1 = fields.Integer()
    tang_giam_tb = fields.Float()
    tang_giam_tb_1 = fields.Integer()
    tang_giam_yeu = fields.Float()
    tang_giam_yeu_1 = fields.Integer()
    tang_giam_cabiet = fields.Float()
    tang_giam_cabiet_1 = fields.Integer()
    tang_giam_kocodiem = fields.Float()
    tang_giam_kocodiem_1 = fields.Integer()

    @api.multi
    @api.depends('ten_trungtam', 'tuan_trungtam', 'thang')
    def _depends_theotuan_diem_trungtam(self):
        for rec in self:
            full = rec.env['danhsach.nhapdiem'].search(
                [('nam_danhgia', '=', rec.nam_trungtam),
                 ('thang_danhgia', '=', rec.thang),
                 ('tuan_danhsach', '=', rec.tuan_trungtam),
                 ('trungtam_danhsach', '=', rec.ten_trungtam.id)])
            rec.tong_songuoi = len(full)

            kha = 0
            trungbinh = 0
            yeu = 0
            cabiet = 0
            chuaco_diem = 0
            for ie in full:
                if ie.kha_b == True:
                    kha += 1

                if ie.trungbinh_b == True:
                    trungbinh += 1

                if ie.yeukem_b == True:
                    yeu += 1

                if ie.ca_biet == True:
                    cabiet += 1

                if ie.chua_codiem == True:
                    chuaco_diem += 1

            if cabiet:
                rec.phantram_cabiet = round((float(cabiet) / float(len(full)) * 100), 1)
            if chuaco_diem:
                rec.phantram_khongcodiem = round((float(chuaco_diem) / float(len(full)) * 100), 1)
            if kha:
                rec.phantram_kha = round((float(kha) / float(len(full)) * 100), 1)
            if trungbinh:
                rec.phantram_tb = round((float(trungbinh) / float(len(full)) * 100), 1)
            if yeu:
                rec.phantram_yeu = round((float(yeu) / float(len(full)) * 100), 1)

            rec.songuoi_kha = kha
            rec.songuoi_tb = trungbinh
            rec.songuoi_yeu = yeu
            rec.songuoi_cabiet = cabiet
            rec.songuoi_chuacodiem = chuaco_diem

from vatnumber import check_vat_ua

from odoo import models, fields, api
from datetime import datetime
import requests
import logging
import codecs
from tempfile import TemporaryFile, NamedTemporaryFile
from io import BytesIO, StringIO
from odoo.exceptions import ValidationError
from docxtpl import DocxTemplate, Listing

_logger = logging.getLogger(__name__)


def date_time_in_jp(day=None, month=None, year=None):
    if not day:
        if not month:
            return u'%s年' % year
        else:
            return u'%s年%s月' % (year, month)
    else:
        return u'%s年%s月%s日' % (year, month, day)


class diemcuoicung(models.Model):
    _name = 'diem.cuoicung'
    _rec_name = 'ten_hocsinh'

    ten_hocsinh = fields.Many2one('lop.thuctapsinh')
    thang_danhgia_n5 = fields.Date('Tháng')
    tuvung_lop = fields.Integer(string='Từ vựng')
    doc_nguphap_lop = fields.Integer(string='Đọc - ngữ pháp')
    nghe_hieu_lop = fields.Integer(string='Nghe hiểu')
    hoi_thoai_lop = fields.Integer(string='Hội thoại')
    tong_lop = fields.Integer(string='Tổng', compute='cal_total_tong_lop_n5', store=True)
    do_lop = fields.Boolean(string='Đỗ', compute='depends_do_lop_n5')
    danhgia_n5 = fields.Char(string='Đánh giá')

    @api.multi
    @api.depends('tuvung_lop', 'doc_nguphap_lop', 'nghe_hieu_lop', 'hoi_thoai_lop')
    def cal_total_tong_lop_n5(self):
        for i in self:
            if i.tuvung_lop > 60 or i.doc_nguphap_lop > 60 or i.nghe_hieu_lop > 60 or i.hoi_thoai_lop > 100:
                raise ValidationError(u"Từ vựng,ngữ pháp,nghe hiểu nhỏ hơn 60, Hội thoại nhỏ hơn 100")
            elif i.tuvung_lop < 0 or i.doc_nguphap_lop < 0 or i.nghe_hieu_lop < 0 or i.hoi_thoai_lop < 0:
                raise ValidationError(u"Điểm phải lớn hơn 0")
            elif i.tuvung_lop <= 60 and i.doc_nguphap_lop <= 60 and i.nghe_hieu_lop <= 60 and i.hoi_thoai_lop <= 100:
                i.tong_lop = i.tuvung_lop + i.doc_nguphap_lop + i.nghe_hieu_lop + i.hoi_thoai_lop
            else:
                i.tong_lop = 0

    @api.multi
    @api.depends('tuvung_lop', 'doc_nguphap_lop', 'nghe_hieu_lop', 'hoi_thoai_lop')
    def depends_do_lop_n5(self):
        for i in self:
            if i.tuvung_lop >= 19 and i.doc_nguphap_lop >= 19 and i.nghe_hieu_lop >= 19 and i.hoi_thoai_lop >= 50:
                i.do_lop = True
            else:
                i.do_lop = False


class LopTTS(models.Model):
    _name = 'lop.thuctapsinh'
    _rec_name = 'tentss_thuctapsinh'
    _order = 'id asc'

    tentss_thuctapsinh = fields.Many2one(comodel_name='listtts.listtts', string='Tên thực tập sinh')
    tenlop_thuctapsinh = fields.Many2one(comodel_name='taolop.taolop', string='Tên lớp')

    trungtam_lop = fields.Many2one(comodel_name='trungtam.daotao', string='Trung tâm đào tạo',
                                   related='tenlop_thuctapsinh.trungtam_taolop')

    trangthai = fields.Selection(selection=[('1', 'Đang học'), ('2', 'Dừng học'), ('3', 'Đã bay')], string='Trạng thái',
                                 default='1', related='tenlop_thuctapsinh.trangthai')

    chunhiem_lop = fields.Many2one('res.users', 'Giáo viên chủ nhiệm', related='tenlop_thuctapsinh.giaovienchunhiem')
    avatar = fields.Binary("Ảnh", related='tentss_thuctapsinh.avatar')

    batdau_lop = fields.Date('Ngày bắt đầu')

    nghiepdoan_tiepnhan = fields.Many2one(comodel_name='nghiepdoan', string='Nghiệp đoàn tiếp nhận')

    phongban = fields.Many2one(comodel_name='department', string='Phòng ban', )

    xinghiep_tiepnhan = fields.Many2one(comodel_name='xinghiep', string='Xí nghiệp tiếp nhận')
    job_jp = fields.Many2one(comodel_name='nganhnghe', string="Ngành nghề", related='tentss_thuctapsinh.job_jp')
    place_to_work = fields.Char(string='Địa điểm làm việc', related='tentss_thuctapsinh.place_to_work')

    lichhoc_lop = fields.Date('Dự kiến xuất cảnh')

    @api.onchange('batdau_lop')
    def onchange_tentss_thuctapsinh_tt(self):
        for i in self:
            if i.tentss_thuctapsinh:
                i.nghiepdoan_tiepnhan = i.tentss_thuctapsinh.nghiepdoan
                i.phongban = i.tentss_thuctapsinh.department
                i.xinghiep_tiepnhan = i.tentss_thuctapsinh.xinghiep
                i.lichhoc_lop = i.tentss_thuctapsinh.date_departure

    thang_nam = fields.Char(string="Test", compute="onchange_method_lichhoc_lop")

    @api.multi
    @api.depends('lichhoc_lop')
    def onchange_method_lichhoc_lop(self):
        for thang in self:
            thang.thang_nam = 'DK_NC_' + str(thang.lichhoc_lop)[0:7]

    # ------Đánh giá vỡ lòng-----------------------------------------
    thoigian_danhgia = fields.Selection(string='Thời gian',
                                        selection=[('A', 'A'), ('B', 'B'), ('C', 'C'), ('D', 'D'), ('E', 'E'),
                                                   ('F', 'F')])
    suckhoe_danhgia = fields.Selection(string='Sức khỏe',
                                       selection=[('A', 'A'), ('B', 'B'), ('C', 'C'), ('D', 'D'), ('E', 'E'),
                                                  ('F', 'F')])
    kiluat_danhgia = fields.Selection(string='Kỷ luật',
                                      selection=[('A', 'A'), ('B', 'B'), ('C', 'C'), ('D', 'D'), ('E', 'E'),
                                                 ('F', 'F')])

    danhgia_volong = fields.Many2many(comodel_name='danhgia.volong', string='Đánh giá về học tập')
    danhgia_ythuctot_volong = fields.Many2many(comodel_name='nhanxet.ythuc', string='Đánh giá ý thức tốt')
    danhgia_ythucxau_volong = fields.Many2many(comodel_name='nhanxet.ythucxau', string='Đánh giá ý thức xấu')
    danhsach_thuctapsinh_volong = fields.One2many(comodel_name='danhsach.nhapdiem',
                                                  inverse_name='danhsach_thuctapsinh_volong',
                                                  string='Đánh giá năng lực tiếng nhật vỡ lòng')

    danhgia_nangluc_tts_lop = fields.One2many(comodel_name='danhsach.nhapdiem', inverse_name='danhsach_thuctapsinh',
                                              string='Đánh giá năng lực tiếng nhật', order='thang_danhgia',
                                              domain=[('volong_volong', '=', False)])

    danhgia_danhgia_lop = fields.One2many(comodel_name='danhsach.nhapdiem', inverse_name='danhgia_baihoc',
                                          string='Đánh giá')

    thaido_tts_lop = fields.One2many(comodel_name='thaido.thaido', inverse_name='thaido', string='Thái độ')
    # ------- Đánh giá N5 ------------------------------------------------------
    diem_n5 = fields.One2many(comodel_name='diem.cuoicung', inverse_name='ten_hocsinh', string='Đánh giá N5')

    @api.multi
    def baocao_button(self):
        return {
            'type': 'ir.actions.act_url',
            'url': '/trungtam/%s' % (self.id),
            'target': 'self', }

    def baocao_hoso(self, document):
        docs = self.env['intern.document'].search([('name', '=', "InfoTTS")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].attachment, "base64"))
            docdata = DocxTemplate(stream)
            context = {}

            if document.tentss_thuctapsinh.avatar is not None:
                try:
                    streamAvatar = BytesIO(document.tentss_thuctapsinh.avatar.decode("base64"))
                    docdata.replace_pic('avatar.jpg', streamAvatar)
                except:
                    pass

            context[
                'hhjp_0001'] = document.tentss_thuctapsinh.name_in_japan if document.tentss_thuctapsinh.name_in_japan else ''
            context[
                'hhjp_0097'] = document.nghiepdoan_tiepnhan.name_china if document.nghiepdoan_tiepnhan.name_china else ''
            context[
                'hhjp_0010'] = document.xinghiep_tiepnhan.name_china if document.xinghiep_tiepnhan.name_china else ''
            context['hhjp_0115'] = document.job_jp if document.job_jp else ''

            # Ngày tháng năm sinh
            if document.tentss_thuctapsinh.date_of_birth:
                context['hhjp_0004'] = date_time_in_jp(document.tentss_thuctapsinh.date_of_birth.day,
                                                       document.tentss_thuctapsinh.date_of_birth.month,
                                                       document.tentss_thuctapsinh.date_of_birth.year)
            else:
                context['hhjp_0004'] = u'年　月　日'
            context[
                'csdt_0001'] = document.tenlop_thuctapsinh.tenlop_taolop if document.tenlop_thuctapsinh.tenlop_taolop else ''
            context[
                'csdt_0003'] = document.chunhiem_lop.partner_id.name if document.chunhiem_lop.partner_id.name else ''

            if document.batdau_lop:
                context['csdt_0002'] = date_time_in_jp(document.batdau_lop.day,
                                                       document.batdau_lop.month,
                                                       document.batdau_lop.year)
            else:
                context['csdt_0002'] = u'年　月　日'
            if document.lichhoc_lop:
                context['hhjp_0902'] = date_time_in_jp(document.lichhoc_lop.day,
                                                       document.lichhoc_lop.month,
                                                       document.lichhoc_lop.year)
            else:
                context['hhjp_0902'] = u'年　月　日'

            diem_volong = 0
            sosanh = 0
            do = []
            khongdo = []
            for tts_do in document.danhsach_thuctapsinh_volong:
                if tts_do.do_volong == True:
                    do.append(tts_do)
                else:
                    khongdo.append(tts_do)
            if len(do) > 0:
                for volong in do:
                    sosanh = 0 + diem_volong
                    diem_volong = volong.tong_volong
                    if diem_volong >= sosanh:
                        context['csdt_0004'] = volong.tuvung_volong if volong.tuvung_volong else '*'
                        context['csdt_0005'] = volong.nguphap_volong if volong.nguphap_volong else '*'
                        context['csdt_0009'] = volong.tong_volong if volong.tong_volong else '*'
                        context['csdt_0010'] = u'✔'
                        context['csdt_0011'] = u''
            else:
                if len(khongdo) > 0:
                    for volong in khongdo:
                        sosanh = 0 + diem_volong
                        diem_volong = volong.tong_volong
                        if diem_volong >= sosanh:
                            context['csdt_0004'] = volong.tuvung_volong if volong.tuvung_volong else '*'
                            context['csdt_0005'] = volong.nguphap_volong if volong.nguphap_volong else '*'
                            context['csdt_0009'] = volong.tong_volong if volong.tong_volong else '*'
                            context['csdt_0010'] = u''
                            context['csdt_0011'] = u'✔'

            context['csdt_0012'] = document.thoigian_danhgia if document.thoigian_danhgia else '*'
            context['csdt_0013'] = document.suckhoe_danhgia if document.suckhoe_danhgia else '*'
            context['csdt_0014'] = document.kiluat_danhgia if document.kiluat_danhgia else '*'

            danhgia_ythuctot_volong = ''
            for item in document.danhgia_ythuctot_volong:
                danhgia_ythuctot_volong = danhgia_ythuctot_volong + '\n' + u'・' + (item.name)

            danhgia_ythucxau_volong = ''
            for item in document.danhgia_ythucxau_volong:
                danhgia_ythucxau_volong = danhgia_ythucxau_volong + '\n' + u'・' + (item.name)
            if danhgia_ythuctot_volong:
                danhgia = danhgia_ythuctot_volong
            elif danhgia_ythucxau_volong:
                danhgia = danhgia_ythucxau_volong
            elif danhgia_ythuctot_volong and danhgia_ythucxau_volong:
                danhgia = danhgia_ythuctot_volong + ' %s' % (
                    danhgia_ythucxau_volong)
            else:
                danhgia = '*'

            context['csdt_0015'] = Listing(danhgia)

            danhgia_volong = ''
            for item in document.danhgia_volong:
                danhgia_volong = danhgia_volong + '\n' + u'・' + (item.name)
            context['csdt_0016'] = Listing(danhgia_volong) if danhgia_volong else '*'

            table_daotao = []
            thang = []
            for danhgia_nangluc in document.thaido_tts_lop:
                if danhgia_nangluc.thang not in thang:
                    thang.append(danhgia_nangluc.thang)
                    infor_danhgia_nangluc = {}
                    infor_danhgia_nangluc['csdt_0019'] = danhgia_nangluc.thang if danhgia_nangluc.thang else ''
                    table_baihoc_thang = []
                    item = 1
                    for baihoc_thang in document.danhgia_nangluc_tts_lop:
                        if baihoc_thang.thang_danhgia == danhgia_nangluc.thang:
                            if item == 1:
                                infor_danhgia_nangluc[
                                    'csdt_0020_1'] = baihoc_thang.baihoc_danhgia.baihocdanhgia if baihoc_thang.baihoc_danhgia.baihocdanhgia else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0021_1'] = baihoc_thang.tuvung_1 if baihoc_thang.tuvung_1 else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0022_1'] = baihoc_thang.nguphap if baihoc_thang.nguphap else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0023_1'] = baihoc_thang.nghe if baihoc_thang.nghe else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0024_1'] = baihoc_thang.hoithoai if baihoc_thang.hoithoai else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0025_1'] = baihoc_thang.tiengnhat if baihoc_thang.tiengnhat else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0026_1'] = baihoc_thang.tong if baihoc_thang.tong else '*'
                                item += 1;
                            elif item == 2:
                                infor_danhgia_nangluc[
                                    'csdt_0020_2'] = baihoc_thang.baihoc_danhgia.baihocdanhgia if baihoc_thang.baihoc_danhgia.baihocdanhgia else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0021_2'] = baihoc_thang.tuvung_1 if baihoc_thang.tuvung_1 else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0022_2'] = baihoc_thang.nguphap if baihoc_thang.nguphap else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0023_2'] = baihoc_thang.nghe if baihoc_thang.nghe else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0024_2'] = baihoc_thang.hoithoai if baihoc_thang.hoithoai else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0025_2'] = baihoc_thang.tiengnhat if baihoc_thang.tiengnhat else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0026_2'] = baihoc_thang.tong if baihoc_thang.tong else '*'
                                item += 1;
                            elif item == 3:
                                infor_danhgia_nangluc[
                                    'csdt_0020_3'] = baihoc_thang.baihoc_danhgia.baihocdanhgia if baihoc_thang.baihoc_danhgia.baihocdanhgia else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0021_3'] = baihoc_thang.tuvung_1 if baihoc_thang.tuvung_1 else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0022_3'] = baihoc_thang.nguphap if baihoc_thang.nguphap else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0023_3'] = baihoc_thang.nghe if baihoc_thang.nghe else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0024_3'] = baihoc_thang.hoithoai if baihoc_thang.hoithoai else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0025_3'] = baihoc_thang.tiengnhat if baihoc_thang.tiengnhat else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0026_3'] = baihoc_thang.tong if baihoc_thang.tong else '*'
                                item += 1;
                            elif item == 4:
                                infor_danhgia_nangluc[
                                    'csdt_0020_4'] = baihoc_thang.baihoc_danhgia.baihocdanhgia if baihoc_thang.baihoc_danhgia.baihocdanhgia else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0021_4'] = baihoc_thang.tuvung_1 if baihoc_thang.tuvung_1 else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0022_4'] = baihoc_thang.nguphap if baihoc_thang.nguphap else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0023_4'] = baihoc_thang.nghe if baihoc_thang.nghe else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0024_4'] = baihoc_thang.hoithoai if baihoc_thang.hoithoai else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0025_4'] = baihoc_thang.tiengnhat if baihoc_thang.tiengnhat else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0026_4'] = baihoc_thang.tong if baihoc_thang.tong else '*'
                                break
                    for thaido_tts_lop in document.thaido_tts_lop:
                        if thaido_tts_lop.thang == danhgia_nangluc.thang:
                            infor_danhgia_nangluc[
                                'csdt_0028'] = thaido_tts_lop.thoigian if thaido_tts_lop.thoigian else '*'
                            infor_danhgia_nangluc[
                                'csdt_0029'] = thaido_tts_lop.tinhthan if thaido_tts_lop.tinhthan else '*'
                            infor_danhgia_nangluc['csdt_0030'] = thaido_tts_lop.kiluat if thaido_tts_lop.kiluat else '*'
                            infor_danhgia_nangluc[
                                'csdt_0017'] = thaido_tts_lop.vang_danhgia if thaido_tts_lop.vang_danhgia else ''

                            lydo_danhgia = ''
                            for item in thaido_tts_lop.lydo_danhgia:
                                lydo_danhgia = lydo_danhgia + '\n' + u'・' + (item.name)
                            infor_danhgia_nangluc['csdt_0018'] = Listing(lydo_danhgia) if lydo_danhgia else '*'

                            nhanxet_hoctaptot = ''
                            for item in thaido_tts_lop.nhanxet_hoctaptot:
                                nhanxet_hoctaptot = nhanxet_hoctaptot + '\n' + u'・' + (item.name)
                            nhanxet_hoctapxau = ''
                            for item in thaido_tts_lop.nhanxet_hoctapxau:
                                nhanxet_hoctapxau = nhanxet_hoctapxau + '\n' + u'・' + (item.name)

                            if thaido_tts_lop.nhanxet_hoctaptot and thaido_tts_lop.nhanxet_hoctapxau:
                                danhgia_nangluc_1 = nhanxet_hoctaptot + u'・' + (nhanxet_hoctapxau)
                            elif thaido_tts_lop.nhanxet_hoctaptot:
                                danhgia_nangluc_1 = '%s' % (nhanxet_hoctaptot)
                            elif thaido_tts_lop.nhanxet_hoctapxau:
                                danhgia_nangluc_1 = '%s' % (nhanxet_hoctapxau)
                            else:
                                danhgia_nangluc_1 = '*'

                            infor_danhgia_nangluc['csdt_0027'] = Listing(danhgia_nangluc_1)

                            nhanxet_ythuctot = ''
                            for item in thaido_tts_lop.nhanxet_ythuctot:
                                nhanxet_ythuctot = nhanxet_ythuctot + '\n' + u'・' + (item.name)
                            nhanxet_ythucxau = ''
                            for item in thaido_tts_lop.nhanxet_ythucxau:
                                nhanxet_ythucxau = nhanxet_ythucxau + '\n' + u'・' + (item.name)

                            if thaido_tts_lop.nhanxet_ythuctot and thaido_tts_lop.nhanxet_ythucxau:
                                csdt_0031 = nhanxet_ythuctot + u'・' + (nhanxet_ythucxau)
                            elif thaido_tts_lop.nhanxet_ythuctot:
                                csdt_0031 = nhanxet_ythuctot
                            elif thaido_tts_lop.nhanxet_ythucxau:
                                csdt_0031 = nhanxet_ythucxau
                            else:
                                csdt_0031 = '*'

                            infor_danhgia_nangluc['csdt_0031'] = Listing(csdt_0031)
                    table_daotao.append(infor_danhgia_nangluc)
            context['tbl_baihoc'] = table_daotao

            table_diem_n5 = []
            for i in document.diem_n5:
                diem = {}
                if i.thang_danhgia_n5:
                    diem['csdt_0041'] = date_time_in_jp(i.thang_danhgia_n5.day,
                                                        i.thang_danhgia_n5.month,
                                                        i.thang_danhgia_n5.year)
                else:
                    diem['csdt_0041'] = u'年　月　日'
                diem['csdt_0032'] = i.tuvung_lop if i.tuvung_lop else ''
                diem['csdt_0033'] = i.doc_nguphap_lop if i.doc_nguphap_lop else ''
                diem['csdt_0034'] = i.nghe_hieu_lop if i.nghe_hieu_lop else ''
                diem['csdt_0035'] = i.hoi_thoai_lop if i.hoi_thoai_lop else ''
                diem['csdt_0036'] = i.tong_lop if i.tong_lop else ''
                diem['csdt_0039'] = i.danhgia_n5 if i.danhgia_n5 else ''
                if i.tong_lop or i.thang_danhgia_n5:
                    if i.do_lop == True:
                        diem['csdt_0037'] = u'✔'
                    else:
                        diem['csdt_0038'] = u'✔'
                else:
                    diem['csdt_0037'] = ''
                    diem['csdt_0038'] = ''
                table_diem_n5.append(diem)
            context['tbl_n5'] = table_diem_n5
            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None


    @api.multi
    def laydulieu_button_trungtam(self):
        for i in self:
            if i.tentss_thuctapsinh:
                i.nghiepdoan_tiepnhan = i.tentss_thuctapsinh.nghiepdoan
                i.phongban = i.tentss_thuctapsinh.department
                i.xinghiep_tiepnhan = i.tentss_thuctapsinh.xinghiep
                i.lichhoc_lop = i.tentss_thuctapsinh.date_departure
        url = "https://cms-elearning.kantanshosaku.jp/api/tts"
        diem_tudong = self.env['lop.thuctapsinh'].search([('trangthai', '=', '1')])
        for i in diem_tudong:
            if i.tentss_thuctapsinh.custom_id:
                id_inte = i.tentss_thuctapsinh.custom_id
                payload = {'matts': id_inte}
                files = []
                headers = {}
                response = requests.request("POST", url, headers=headers, data=payload, files=files)
                if response:
                    demo1 = response.json()
                    diem_n5 = demo1['diem_N5']
                    diem_n4 = demo1['diem_N4']

                    demo_n4 = []
                    for n4 in range(0, len(diem_n4)):
                        kiemtra_ketthuc1_n4 = diem_n4[n4]['namecoursecontent']
                        kiemtra_ketthuc_n4 = str(kiemtra_ketthuc1_n4.lower())
                        demo1_n4 = u'kiểm tra kết thúc'
                        if not kiemtra_ketthuc_n4.rfind(str(demo1_n4)):
                            demo_n4.append(diem_n4[n4])
                    if len(demo_n4) > 0:
                        ngay_gi = datetime.today()
                        date_now = ngay_gi.isocalendar()[1]
                        nam_cms = int(ngay_gi.year)

                        ngay = []
                        for item2 in range(0, len(demo_n4)):
                            ngaytao = demo_n4[item2]['created_at']
                            ngaytao_1 = \
                                datetime(int(ngaytao[0:4]), int(ngaytao[5:7]), int(ngaytao[8:10])).isocalendar()[1]
                            nam_ngaytao = int(ngaytao[0:4])
                            if nam_cms == nam_ngaytao:
                                if ngaytao_1 == date_now:
                                    ngay.append(demo_n4[item2])
                        # ----từ vựng-----
                        tuvung = 0
                        tu_vung = 0
                        tu_vung_len = 0
                        # ----Ngữ pháp----
                        nguphap = 0
                        ngu_phap = 0
                        tu_ngu_phap = 0
                        # -----Nghe-----
                        nghe = 0
                        nghe_n5 = 0
                        nghe_n5_len = 0
                        # ----hội thoại----
                        hoithoai = 0
                        hoi_thoai = 0
                        hoi_thoai_len = 0
                        # ---- đời sống ---
                        doisong = 0
                        doi_song = 0
                        doi_song_len = 0
                        if len(ngay) > 0:
                            for item3 in range(0, len(ngay)):
                                nameoftest1 = ngay[item3]['nameoftest']
                                nameoftest = nameoftest1.lower()
                                diem = ngay[item3]['diem']
                                re_1 = u"từ vựng"
                                re_2 = u"ngữ pháp"
                                re_3 = u'nghe'
                                re_4 = u'hội thoại'
                                re_5 = u'đời sống'
                                return1 = nameoftest.rfind(str(re_1))
                                return2 = nameoftest.rfind(str(re_2))
                                return3 = nameoftest.rfind(str(re_3))
                                return4 = nameoftest.rfind(str(re_4))
                                return5 = nameoftest.rfind(str(re_5))

                                if return1 == -1:
                                    print('tv')
                                else:
                                    tu_vung += diem
                                    tu_vung_len += 1

                                if return2 == -1:
                                    print('np')
                                else:
                                    ngu_phap += diem
                                    tu_ngu_phap += 1

                                if return3 == -1:
                                    print('n')
                                else:
                                    nghe_n5 += diem
                                    nghe_n5_len += 1

                                if return4 == -1:
                                    print('ht')
                                else:
                                    hoi_thoai += diem
                                    hoi_thoai_len += 1

                                if return5 == -1:
                                    print('ds')
                                else:
                                    doi_song += diem
                                    doi_song_len += 1

                            if tu_vung_len > 0:
                                tuvung = round((float(tu_vung) / float(tu_vung_len)), 0)
                            else:
                                tuvung = 0

                            if tu_ngu_phap > 0:
                                nguphap = round((float(ngu_phap) / float(tu_ngu_phap)), 0)
                            else:
                                nguphap = 0

                            if nghe_n5_len > 0:
                                nghe = round((float(nghe_n5) / float(nghe_n5_len)), 0)
                            else:
                                nghe = 0

                            if hoi_thoai_len > 0:
                                hoithoai = round((float(hoi_thoai) / float(hoi_thoai_len)), 0)
                            else:
                                hoithoai = 0

                            if doi_song_len > 0:
                                doisong = round((float(doi_song) / float(doi_song_len)), 0)
                            else:
                                doisong = 0
                            thang = str(ngay_gi.month)
                            nam = str(ngay_gi.year)
                            ds_nhapdiem = self.env['nhapdiem.nhapdiem'].search(
                                [('tenlop_nhapdiem', '=', i.tenlop_thuctapsinh.id),
                                 ('nam_nhapdiem', '=', nam),
                                 ('thang_nhapdiem', '=', thang)])
                            ds = i.env['danhsach.nhapdiem'].search(
                                [('danhsach_thuctapsinh', '=', i.id),
                                 ('nam_danhgia', '=', nam),
                                 ('thang_danhgia', '=', thang)])
                            ds_trungtam = i.env['trungtam.diem'].search(
                                [('ten_trungtam', '=', i.trungtam_lop.id),
                                 ('nam_trungtam', '=', nam),
                                 ('thang', '=', thang)])
                            if len(ds) > 3:
                                nhpdiem = i.env['nhapdiem.nhapdiem'].search(
                                    [('tenlop_nhapdiem', '=', i.tenlop_thuctapsinh.id),
                                     ('nam_nhapdiem', '=', nam),
                                     ('thang_nhapdiem', '=', thang),
                                     ('tuan_nhapdiem', '=', '4')])

                                # Thêm phần lớp thực tập sinh
                                tuan_4 = i.env['danhsach.nhapdiem'].search(
                                    [('danhsach_thuctapsinh', '=', i.id),
                                     ('nam_danhgia', '=', nam),
                                     ('thang_danhgia', '=', thang),
                                     ('tuan_danhsach', '=', '4')])

                                tuan_4.tuvung_1 = (tuan_4.tuvung_1 + tuvung) / 2
                                tuan_4.nguphap = (tuan_4.nguphap + nguphap) / 2
                                tuan_4.nghe = (tuan_4.nghe + nghe) / 2
                                tuan_4.hoithoai = (tuan_4.hoithoai + hoithoai) / 2
                                tuan_4.tiengnhat = (tuan_4.tiengnhat + doisong) / 2
                                tuan_4.tentss_danhsach = i.tentss_thuctapsinh.id
                                tuan_4.tenlop_danhsach = i.tenlop_thuctapsinh.id
                                tuan_4.danhsach_thuctapsinh = i.id
                                # tuan_4.ngaybc = datetime.today()
                                tuan_4.ngaybc = datetime.date(ngay_gi)

                                tuan_4.thang_danhgia = thang
                                tuan_4.nam_danhgia = nam
                                tuan_4.tuan_danhsach = '4'
                                tuan_4.danhsach_tts = nhpdiem.id

                            elif len(ds) <= 3:
                                # Thêm Phần tình hình học tập Trung Tâm
                                vals_trungtam_row = {}
                                vals_trungtam_row['ten_trungtam'] = i.trungtam_lop.id
                                vals_trungtam_row['thang'] = thang
                                vals_trungtam_row['nam_trungtam'] = nam
                                if len(ds_trungtam) < 1:
                                    vals_trungtam_row['tuan_trungtam'] = '1'
                                elif len(ds_trungtam) == 1:
                                    vals_trungtam_row['tuan_trungtam'] = '2'
                                elif len(ds_trungtam) == 2:
                                    vals_trungtam_row['tuan_trungtam'] = '3'
                                elif len(ds_trungtam) == 3:
                                    vals_trungtam_row['tuan_trungtam'] = '4'
                                vals_trungtam_row['ngay_tao'] = datetime.today()

                                nhpdiem_trungtam = i.env['trungtam.diem'].search(
                                    [('ten_trungtam', '=', i.trungtam_lop.id),
                                     ('nam_trungtam', '=', nam),
                                     ('thang', '=', thang),
                                     ('ngay_tao', '=', vals_trungtam_row['ngay_tao'])])

                                if len(nhpdiem_trungtam) >= 1:
                                    nhpdiem_trungtam = nhpdiem_trungtam
                                else:
                                    i.env['trungtam.diem'].create(vals_trungtam_row)

                                # Thêm phần báo cáo
                                vals_baocao_row = {}
                                vals_baocao_row['tenlop_nhapdiem'] = i.tenlop_thuctapsinh.id
                                vals_baocao_row['thang_nhapdiem'] = thang
                                vals_baocao_row['nam_nhapdiem'] = nam
                                if len(ds_nhapdiem) < 1:
                                    vals_baocao_row['tuan_nhapdiem'] = '1'
                                elif len(ds_nhapdiem) == 1:
                                    vals_baocao_row['tuan_nhapdiem'] = '2'
                                elif len(ds_nhapdiem) == 2:
                                    vals_baocao_row['tuan_nhapdiem'] = '3'
                                elif len(ds_nhapdiem) == 3:
                                    vals_baocao_row['tuan_nhapdiem'] = '4'
                                vals_baocao_row['tuan'] = datetime.today()

                                nhpdiem = i.env['nhapdiem.nhapdiem'].search(
                                    [('tenlop_nhapdiem', '=', i.tenlop_thuctapsinh.id),
                                     ('nam_nhapdiem', '=', nam),
                                     ('thang_nhapdiem', '=', thang),
                                     ('tuan', '=', vals_baocao_row['tuan'])])

                                if len(nhpdiem) >= 1:
                                    nhpdiem = nhpdiem
                                else:
                                    nhpdiem = i.env['nhapdiem.nhapdiem'].create(vals_baocao_row)

                                # Thêm phần lớp thực tập sinh
                                vals_diem_row = {}
                                vals_diem_row['tuvung_1'] = tuvung
                                vals_diem_row['nguphap'] = nguphap
                                vals_diem_row['nghe'] = nghe
                                vals_diem_row['hoithoai'] = hoithoai
                                vals_diem_row['tiengnhat'] = doisong
                                vals_diem_row['tentss_danhsach'] = i.tentss_thuctapsinh.id
                                vals_diem_row['tenlop_danhsach'] = i.tenlop_thuctapsinh.id
                                vals_diem_row['danhsach_thuctapsinh'] = i.id
                                vals_diem_row['ngaybc'] = datetime.today()

                                vals_diem_row['thang_danhgia'] = thang
                                vals_diem_row['nam_danhgia'] = nam
                                if len(ds) < 1:
                                    vals_diem_row['tuan_danhsach'] = '1'
                                elif len(ds) == 1:
                                    vals_diem_row['tuan_danhsach'] = '2'
                                elif len(ds) == 2:
                                    vals_diem_row['tuan_danhsach'] = '3'
                                elif len(ds) == 3:
                                    vals_diem_row['tuan_danhsach'] = '4'
                                vals_diem_row['danhsach_tts'] = nhpdiem.id

                                diem_row = i.env['danhsach.nhapdiem'].create(vals_diem_row)
                                i.danhgia_nangluc_tts_lop = [(4, diem_row.id)]
                        else:
                            thang = str(ngay_gi.month)
                            nam = str(ngay_gi.year)
                            ds_nhapdiem = self.env['nhapdiem.nhapdiem'].search(
                                [('tenlop_nhapdiem', '=', i.tenlop_thuctapsinh.id),
                                 ('nam_nhapdiem', '=', nam),
                                 ('thang_nhapdiem', '=', thang)])
                            ds = i.env['danhsach.nhapdiem'].search(
                                [('danhsach_thuctapsinh', '=', i.id),
                                 ('nam_danhgia', '=', nam),
                                 ('thang_danhgia', '=', thang)])
                            ds_trungtam = i.env['trungtam.diem'].search(
                                [('ten_trungtam', '=', i.trungtam_lop.id),
                                 ('nam_trungtam', '=', nam),
                                 ('thang', '=', thang)])
                            if len(ds) > 3:
                                nhpdiem = i.env['nhapdiem.nhapdiem'].search(
                                    [('tenlop_nhapdiem', '=', i.tenlop_thuctapsinh.id),
                                     ('nam_nhapdiem', '=', nam),
                                     ('thang_nhapdiem', '=', thang),
                                     ('tuan_nhapdiem', '=', '4')])

                                # Thêm phần lớp thực tập sinh
                                tuan_4 = i.env['danhsach.nhapdiem'].search(
                                    [('danhsach_thuctapsinh', '=', i.id),
                                     ('nam_danhgia', '=', nam),
                                     ('thang_danhgia', '=', thang),
                                     ('tuan_danhsach', '=', '4')])

                                tuan_4.tuvung_1 = (tuan_4.tuvung_1 + tuvung) / 2
                                tuan_4.nguphap = (tuan_4.nguphap + nguphap) / 2
                                tuan_4.nghe = (tuan_4.nghe + nghe) / 2
                                tuan_4.hoithoai = (tuan_4.hoithoai + hoithoai) / 2
                                tuan_4.tiengnhat = (tuan_4.tiengnhat + doisong) / 2
                                # if tuan_4.diem_sosanh == 0 or tuan_4 == False:
                                #     tuan_4.chua_codiem == True
                                # else:
                                #     tuan_4.chua_codiem == False

                                tuan_4.tentss_danhsach = i.tentss_thuctapsinh.id
                                tuan_4.tenlop_danhsach = i.tenlop_thuctapsinh.id
                                tuan_4.danhsach_thuctapsinh = i.id
                                # tuan_4.ngaybc = datetime.today()
                                tuan_4.ngaybc = datetime.date(ngay_gi)

                                tuan_4.thang_danhgia = thang
                                tuan_4.nam_danhgia = nam
                                tuan_4.tuan_danhsach = '4'
                                tuan_4.danhsach_tts = nhpdiem.id

                            elif len(ds) <= 3:
                                # Thêm Phần tình hình học tập Trung Tâm
                                vals_trungtam_row = {}
                                vals_trungtam_row['ten_trungtam'] = i.trungtam_lop.id
                                vals_trungtam_row['thang'] = thang
                                vals_trungtam_row['nam_trungtam'] = nam
                                if len(ds_trungtam) < 1:
                                    vals_trungtam_row['tuan_trungtam'] = '1'
                                elif len(ds_trungtam) == 1:
                                    vals_trungtam_row['tuan_trungtam'] = '2'
                                elif len(ds_trungtam) == 2:
                                    vals_trungtam_row['tuan_trungtam'] = '3'
                                elif len(ds_trungtam) == 3:
                                    vals_trungtam_row['tuan_trungtam'] = '4'
                                vals_trungtam_row['ngay_tao'] = datetime.today()

                                nhpdiem_trungtam = i.env['trungtam.diem'].search(
                                    [('ten_trungtam', '=', i.trungtam_lop.id),
                                     ('nam_trungtam', '=', nam),
                                     ('thang', '=', thang),
                                     ('ngay_tao', '=', vals_trungtam_row['ngay_tao'])])

                                if len(nhpdiem_trungtam) >= 1:
                                    nhpdiem_trungtam = nhpdiem_trungtam
                                else:
                                    i.env['trungtam.diem'].create(vals_trungtam_row)

                                # Thêm phần báo cáo
                                vals_baocao_row = {}
                                vals_baocao_row['tenlop_nhapdiem'] = i.tenlop_thuctapsinh.id
                                vals_baocao_row['thang_nhapdiem'] = thang
                                vals_baocao_row['nam_nhapdiem'] = nam
                                if len(ds_nhapdiem) < 1:
                                    vals_baocao_row['tuan_nhapdiem'] = '1'
                                elif len(ds_nhapdiem) == 1:
                                    vals_baocao_row['tuan_nhapdiem'] = '2'
                                elif len(ds_nhapdiem) == 2:
                                    vals_baocao_row['tuan_nhapdiem'] = '3'
                                elif len(ds_nhapdiem) == 3:
                                    vals_baocao_row['tuan_nhapdiem'] = '4'
                                vals_baocao_row['tuan'] = datetime.today()

                                nhpdiem = i.env['nhapdiem.nhapdiem'].search(
                                    [('tenlop_nhapdiem', '=', i.tenlop_thuctapsinh.id),
                                     ('nam_nhapdiem', '=', nam),
                                     ('thang_nhapdiem', '=', thang),
                                     ('tuan', '=', vals_baocao_row['tuan'])])

                                if len(nhpdiem) >= 1:
                                    nhpdiem = nhpdiem
                                else:
                                    nhpdiem = i.env['nhapdiem.nhapdiem'].create(vals_baocao_row)
                                # Thêm phần lớp thực tập sinh
                                vals_diem_row = {}
                                vals_diem_row['tuvung_1'] = 0
                                vals_diem_row['nguphap'] = 0
                                vals_diem_row['nghe'] = 0
                                vals_diem_row['hoithoai'] = 0
                                vals_diem_row['tiengnhat'] = 0
                                # vals_diem_row['chua_codiem'] = True
                                vals_diem_row['tentss_danhsach'] = i.tentss_thuctapsinh.id
                                vals_diem_row['tenlop_danhsach'] = i.tenlop_thuctapsinh.id
                                vals_diem_row['danhsach_thuctapsinh'] = i.id
                                vals_diem_row['ngaybc'] = datetime.today()

                                vals_diem_row['thang_danhgia'] = thang
                                vals_diem_row['nam_danhgia'] = nam

                                if len(ds) < 1:
                                    vals_diem_row['tuan_danhsach'] = '1'
                                elif len(ds) == 1:
                                    vals_diem_row['tuan_danhsach'] = '2'
                                elif len(ds) == 2:
                                    vals_diem_row['tuan_danhsach'] = '3'
                                elif len(ds) == 3:
                                    vals_diem_row['tuan_danhsach'] = '4'
                                vals_diem_row['danhsach_tts'] = nhpdiem.id

                                diem_row = i.env['danhsach.nhapdiem'].create(vals_diem_row)
                                i.danhgia_nangluc_tts_lop = [(4, diem_row.id)]
                    else:
                        demo = []
                        for item in range(0, len(diem_n5)):
                            kiemtra_ketthuc1 = diem_n5[item]['namecoursecontent']
                            kiemtra_ketthuc = kiemtra_ketthuc1.lower()
                            demo1 = u'kiểm tra kết thúc'
                            if not kiemtra_ketthuc.rfind(demo1):
                                demo.append(diem_n5[item])

                        ngay_gi = datetime.today()

                        date_now = ngay_gi.isocalendar()[1]
                        nam_cms = int(ngay_gi.year)

                        ngay = []
                        for item2 in range(0, len(demo)):
                            ngaytao = demo[item2]['created_at']
                            ngaytao_1 = \
                                datetime(int(ngaytao[0:4]), int(ngaytao[5:7]), int(ngaytao[8:10])).isocalendar()[1]
                            nam_ngaytao = int(ngaytao[0:4])
                            if nam_cms == nam_ngaytao:
                                if ngaytao_1 == date_now:
                                    ngay.append(demo[item2])
                        # ----từ vựng-----
                        tuvung = 0
                        tu_vung = 0
                        tu_vung_len = 0
                        # ----Ngữ pháp----
                        nguphap = 0
                        ngu_phap = 0
                        tu_ngu_phap = 0
                        # -----Nghe-----
                        nghe = 0
                        nghe_n5 = 0
                        nghe_n5_len = 0
                        # ----hội thoại----
                        hoithoai = 0
                        hoi_thoai = 0
                        hoi_thoai_len = 0
                        # ---- đời sống ---
                        doisong = 0
                        doi_song = 0
                        doi_song_len = 0
                        if len(ngay) > 0:
                            for item3 in range(0, len(ngay)):
                                nameoftest1 = ngay[item3]['nameoftest']
                                nameoftest = nameoftest1.lower()
                                diem = ngay[item3]['diem']
                                re_1 = u"từ vựng"
                                re_2 = u"ngữ pháp"
                                re_3 = u'nghe'
                                re_4 = u'hội thoại'
                                re_5 = u'đời sống'
                                return1 = nameoftest.rfind(re_1)
                                return2 = nameoftest.rfind(re_2)
                                return3 = nameoftest.rfind(re_3)
                                return4 = nameoftest.rfind(re_4)
                                return5 = nameoftest.rfind(re_5)

                                if return1 == -1:
                                    print('tv')
                                else:
                                    tu_vung += diem
                                    tu_vung_len += 1

                                if return2 == -1:
                                    print('np')
                                else:
                                    ngu_phap += diem
                                    tu_ngu_phap += 1

                                if return3 == -1:
                                    print('ng')
                                else:
                                    nghe_n5 += diem
                                    nghe_n5_len += 1

                                if return4 == -1:
                                    print('ht')
                                else:
                                    hoi_thoai += diem
                                    hoi_thoai_len += 1

                                if return5 == -1:
                                    print('ds')
                                else:
                                    doi_song += diem
                                    doi_song_len += 1

                            if tu_vung_len > 0:
                                tuvung = round((float(tu_vung) / float(tu_vung_len)), 0)
                            else:
                                tuvung = 0

                            if tu_ngu_phap > 0:
                                nguphap = round((float(ngu_phap) / float(tu_ngu_phap)), 0)
                            else:
                                nguphap = 0

                            if nghe_n5_len > 0:
                                nghe = round((float(nghe_n5) / float(nghe_n5_len)), 0)
                            else:
                                nghe = 0

                            if hoi_thoai_len > 0:
                                hoithoai = round((float(hoi_thoai) / float(hoi_thoai_len)), 0)
                            else:
                                hoithoai = 0

                            if doi_song_len > 0:
                                doisong = round((float(doi_song) / float(doi_song_len)), 0)
                            else:
                                doisong = 0
                            # Phần chung
                            thang = str(ngay_gi.month)
                            nam = str(ngay_gi.year)
                            ds_nhapdiem = self.env['nhapdiem.nhapdiem'].search(
                                [('tenlop_nhapdiem', '=', i.tenlop_thuctapsinh.id),
                                 ('nam_nhapdiem', '=', str(int(nam))),
                                 ('thang_nhapdiem', '=', str(int(thang)))])
                            ds = i.env['danhsach.nhapdiem'].search(
                                [('danhsach_thuctapsinh', '=', i.id),
                                 ('nam_danhgia', '=', str(int(nam))),
                                 ('thang_danhgia', '=', str(int(thang)))])
                            ds_trungtam = i.env['trungtam.diem'].search(
                                [('ten_trungtam', '=', i.trungtam_lop.id),
                                 ('nam_trungtam', '=', str(int(nam))),
                                 ('thang', '=', str(int(thang)))])

                            if len(ds) > 3:
                                nhpdiem = i.env['nhapdiem.nhapdiem'].search(
                                    [('tenlop_nhapdiem', '=', i.tenlop_thuctapsinh.id),
                                     ('nam_nhapdiem', '=', str(int(nam))),
                                     ('thang_nhapdiem', '=', str(int(thang))),
                                     ('tuan_nhapdiem', '=', '4')])

                                # Thêm phần lớp thực tập sinh
                                tuan_4 = i.env['danhsach.nhapdiem'].search(
                                    [('danhsach_thuctapsinh', '=', i.id),
                                     ('nam_danhgia', '=', str(int(nam))),
                                     ('thang_danhgia', '=', str(int(thang))),
                                     ('tuan_danhsach', '=', '4')])

                                tuan_4.tuvung_1 = (tuan_4.tuvung_1 + tuvung) / 2
                                tuan_4.nguphap = (tuan_4.nguphap + nguphap) / 2
                                tuan_4.nghe = (tuan_4.nghe + nghe) / 2
                                tuan_4.hoithoai = (tuan_4.hoithoai + hoithoai) / 2
                                tuan_4.tiengnhat = (tuan_4.tiengnhat + doisong) / 2
                                tuan_4.tentss_danhsach = i.tentss_thuctapsinh.id
                                tuan_4.tenlop_danhsach = i.tenlop_thuctapsinh.id
                                tuan_4.danhsach_thuctapsinh = i.id
                                # tuan_4.ngaybc = datetime.today()
                                tuan_4.ngaybc = datetime.date(ngay_gi)

                                tuan_4.thang_danhgia = str(int(thang))
                                tuan_4.nam_danhgia = str(int(nam))
                                tuan_4.tuan_danhsach = '4'
                                tuan_4.danhsach_tts = nhpdiem.id

                            elif len(ds) <= 3:
                                # Thêm Phần tình hình học tập Trung Tâm
                                vals_trungtam_row = {}
                                vals_trungtam_row['ten_trungtam'] = i.trungtam_lop.id
                                vals_trungtam_row['thang'] = str(int(thang))
                                vals_trungtam_row['nam_trungtam'] = str(int(nam))
                                if len(ds_trungtam) < 1:
                                    vals_trungtam_row['tuan_trungtam'] = '1'
                                elif len(ds_trungtam) == 1:
                                    vals_trungtam_row['tuan_trungtam'] = '2'
                                elif len(ds_trungtam) == 2:
                                    vals_trungtam_row['tuan_trungtam'] = '3'
                                elif len(ds_trungtam) == 3:
                                    vals_trungtam_row['tuan_trungtam'] = '4'
                                vals_trungtam_row['ngay_tao'] = datetime.today()

                                nhpdiem_trungtam = i.env['trungtam.diem'].search(
                                    [('ten_trungtam', '=', i.trungtam_lop.id),
                                     ('nam_trungtam', '=', str(int(nam))),
                                     ('thang', '=', str(int(thang))),
                                     ('ngay_tao', '=', vals_trungtam_row['ngay_tao'])])

                                if len(nhpdiem_trungtam) >= 1:
                                    nhpdiem_trungtam = nhpdiem_trungtam
                                else:
                                    i.env['trungtam.diem'].create(vals_trungtam_row)

                                # Thêm phần báo cáo
                                vals_baocao_row = {}
                                vals_baocao_row['tenlop_nhapdiem'] = i.tenlop_thuctapsinh.id
                                vals_baocao_row['thang_nhapdiem'] = str(int(thang))
                                vals_baocao_row['nam_nhapdiem'] = str(int(nam))
                                if len(ds_nhapdiem) < 1:
                                    vals_baocao_row['tuan_nhapdiem'] = '1'
                                elif len(ds_nhapdiem) == 1:
                                    vals_baocao_row['tuan_nhapdiem'] = '2'
                                elif len(ds_nhapdiem) == 2:
                                    vals_baocao_row['tuan_nhapdiem'] = '3'
                                elif len(ds_nhapdiem) == 3:
                                    vals_baocao_row['tuan_nhapdiem'] = '4'
                                vals_baocao_row['tuan'] = datetime.today()

                                nhpdiem = i.env['nhapdiem.nhapdiem'].search(
                                    [('tenlop_nhapdiem', '=', i.tenlop_thuctapsinh.id),
                                     ('nam_nhapdiem', '=', str(int(nam))),
                                     ('thang_nhapdiem', '=', str(int(thang))),
                                     ('tuan', '=', vals_baocao_row['tuan'])])

                                if len(nhpdiem) >= 1:
                                    nhpdiem = nhpdiem
                                else:
                                    nhpdiem = i.env['nhapdiem.nhapdiem'].create(vals_baocao_row)

                                # Thêm Phần Lớp thực tập sinh
                                vals_diem_row = {}
                                vals_diem_row['tuvung_1'] = tuvung
                                vals_diem_row['nguphap'] = nguphap
                                vals_diem_row['nghe'] = nghe
                                vals_diem_row['hoithoai'] = hoithoai
                                vals_diem_row['tiengnhat'] = doisong
                                vals_diem_row['tentss_danhsach'] = i.tentss_thuctapsinh.id
                                vals_diem_row['tenlop_danhsach'] = i.tenlop_thuctapsinh.id
                                vals_diem_row['danhsach_thuctapsinh'] = i.id
                                vals_diem_row['ngaybc'] = datetime.today()

                                vals_diem_row['thang_danhgia'] = str(int(thang))
                                vals_diem_row['nam_danhgia'] = str(int(nam))
                                if len(ds) < 1:
                                    vals_diem_row['tuan_danhsach'] = '1'
                                elif len(ds) == 1:
                                    vals_diem_row['tuan_danhsach'] = '2'
                                elif len(ds) == 2:
                                    vals_diem_row['tuan_danhsach'] = '3'
                                elif len(ds) == 3:
                                    vals_diem_row['tuan_danhsach'] = '4'
                                vals_diem_row['danhsach_tts'] = nhpdiem.id

                                diem_row = i.env['danhsach.nhapdiem'].create(vals_diem_row)
                                i.danhgia_nangluc_tts_lop = [(4, diem_row.id)]
                        else:
                            thang = str(ngay_gi.month)
                            nam = str(ngay_gi.year)
                            ds_nhapdiem = self.env['nhapdiem.nhapdiem'].search(
                                [('tenlop_nhapdiem', '=', i.tenlop_thuctapsinh.id),
                                 ('nam_nhapdiem', '=', str(int(nam))),
                                 ('thang_nhapdiem', '=', str(int(thang)))])
                            ds = i.env['danhsach.nhapdiem'].search(
                                [('danhsach_thuctapsinh', '=', i.id),
                                 ('nam_danhgia', '=', str(int(nam))),
                                 ('thang_danhgia', '=', str(int(thang)))])
                            ds_trungtam = i.env['trungtam.diem'].search(
                                [('ten_trungtam', '=', i.trungtam_lop.id),
                                 ('nam_trungtam', '=', str(int(nam))),
                                 ('thang', '=', str(int(thang)))])
                            if len(ds) > 3:
                                nhpdiem = i.env['nhapdiem.nhapdiem'].search(
                                    [('tenlop_nhapdiem', '=', i.tenlop_thuctapsinh.id),
                                     ('nam_nhapdiem', '=', str(int(nam))),
                                     ('thang_nhapdiem', '=', str(int(thang))),
                                     ('tuan_nhapdiem', '=', '4')])

                                # Thêm phần lớp thực tập sinh
                                tuan_4 = i.env['danhsach.nhapdiem'].search(
                                    [('danhsach_thuctapsinh', '=', i.id),
                                     ('nam_danhgia', '=', str(int(nam))),
                                     ('thang_danhgia', '=', str(int(thang))),
                                     ('tuan_danhsach', '=', '4')])

                                tuan_4.tuvung_1 = (tuan_4.tuvung_1 + tuvung) / 2
                                tuan_4.nguphap = (tuan_4.nguphap + nguphap) / 2
                                tuan_4.nghe = (tuan_4.nghe + nghe) / 2
                                tuan_4.hoithoai = (tuan_4.hoithoai + hoithoai) / 2
                                tuan_4.tiengnhat = (tuan_4.tiengnhat + doisong) / 2
                                # if tuan_4.diem_sosanh == 0 or tuan_4 == False:
                                #     tuan_4.chua_codiem == True
                                # else:
                                #     tuan_4.chua_codiem == False

                                tuan_4.tentss_danhsach = i.tentss_thuctapsinh.id
                                tuan_4.tenlop_danhsach = i.tenlop_thuctapsinh.id
                                tuan_4.danhsach_thuctapsinh = i.id
                                # tuan_4.ngaybc = datetime.today()
                                tuan_4.ngaybc = datetime.date(ngay_gi)

                                tuan_4.thang_danhgia = str(int(thang))
                                tuan_4.nam_danhgia = str(int(nam))
                                tuan_4.tuan_danhsach = '4'
                                tuan_4.danhsach_tts = nhpdiem.id

                            elif len(ds) <= 3:
                                # Thêm Phần tình hình học tập Trung Tâm
                                vals_trungtam_row = {}
                                vals_trungtam_row['ten_trungtam'] = i.trungtam_lop.id
                                vals_trungtam_row['thang'] = str(int(thang))
                                vals_trungtam_row['nam_trungtam'] = str(int(nam))
                                if len(ds_trungtam) < 1:
                                    vals_trungtam_row['tuan_trungtam'] = '1'
                                elif len(ds_trungtam) == 1:
                                    vals_trungtam_row['tuan_trungtam'] = '2'
                                elif len(ds_trungtam) == 2:
                                    vals_trungtam_row['tuan_trungtam'] = '3'
                                elif len(ds_trungtam) == 3:
                                    vals_trungtam_row['tuan_trungtam'] = '4'
                                vals_trungtam_row['ngay_tao'] = datetime.today()

                                nhpdiem_trungtam = i.env['trungtam.diem'].search(
                                    [('ten_trungtam', '=', i.trungtam_lop.id),
                                     ('nam_trungtam', '=', str(int(nam))),
                                     ('thang', '=', str(int(thang))),
                                     ('ngay_tao', '=', vals_trungtam_row['ngay_tao'])])
                                if len(nhpdiem_trungtam) >= 1:
                                    nhpdiem_trungtam = nhpdiem_trungtam
                                else:
                                    i.env['trungtam.diem'].create(vals_trungtam_row)

                                # Thêm phần báo cáo
                                vals_baocao_row = {}
                                vals_baocao_row['tenlop_nhapdiem'] = i.tenlop_thuctapsinh.id
                                vals_baocao_row['thang_nhapdiem'] = str(int(thang))
                                vals_baocao_row['nam_nhapdiem'] = str(int(nam))
                                if len(ds_nhapdiem) < 1:
                                    vals_baocao_row['tuan_nhapdiem'] = '1'
                                elif len(ds_nhapdiem) == 1:
                                    vals_baocao_row['tuan_nhapdiem'] = '2'
                                elif len(ds_nhapdiem) == 2:
                                    vals_baocao_row['tuan_nhapdiem'] = '3'
                                elif len(ds_nhapdiem) == 3:
                                    vals_baocao_row['tuan_nhapdiem'] = '4'
                                vals_baocao_row['tuan'] = datetime.today()

                                nhpdiem = i.env['nhapdiem.nhapdiem'].search(
                                    [('tenlop_nhapdiem', '=', i.tenlop_thuctapsinh.id),
                                     ('nam_nhapdiem', '=', str(int(nam))),
                                     ('thang_nhapdiem', '=', str(int(thang))),
                                     ('tuan', '=', vals_baocao_row['tuan'])])

                                if len(nhpdiem) >= 1:
                                    nhpdiem = nhpdiem
                                else:
                                    nhpdiem = i.env['nhapdiem.nhapdiem'].create(vals_baocao_row)

                                # Thêm Phần Lớp thực tập sinh
                                vals_diem_row = {}
                                vals_diem_row['tuvung_1'] = 0
                                vals_diem_row['nguphap'] = 0
                                vals_diem_row['nghe'] = 0
                                vals_diem_row['hoithoai'] = 0
                                vals_diem_row['tiengnhat'] = 0
                                # vals_diem_row['chua_codiem'] = True
                                vals_diem_row['tentss_danhsach'] = i.tentss_thuctapsinh.id
                                vals_diem_row['tenlop_danhsach'] = i.tenlop_thuctapsinh.id
                                vals_diem_row['danhsach_thuctapsinh'] = i.id
                                vals_diem_row['ngaybc'] = datetime.today()
                                vals_diem_row['thang_danhgia'] = str(int(thang))
                                vals_diem_row['nam_danhgia'] = str(int(nam))
                                if len(ds) < 1:
                                    vals_diem_row['tuan_danhsach'] = '1'
                                elif len(ds) == 1:
                                    vals_diem_row['tuan_danhsach'] = '2'
                                elif len(ds) == 2:
                                    vals_diem_row['tuan_danhsach'] = '3'
                                elif len(ds) == 3:
                                    vals_diem_row['tuan_danhsach'] = '4'

                                vals_diem_row['danhsach_tts'] = nhpdiem.id

                                diem_row = i.env['danhsach.nhapdiem'].create(vals_diem_row)
                                i.danhgia_nangluc_tts_lop = [(4, diem_row.id)]
            else:
                'Không có mã tts trên cms'


class VoLong(models.Model):
    _name = 'danhgia.volong'
    _rec_name = 'name'

    name = fields.Char('danh gia')
    sequence = fields.Integer('sequence', help='Sequence for the handle.')

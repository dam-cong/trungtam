# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import ValidationError
import datetime


class DSNhapDiem(models.Model):
    _name = 'danhsach.nhapdiem'
    _rec_name = 'tentss_danhsach'

    tenlop_danhsach = fields.Many2one(comodel_name='taolop.taolop', string='Tên lớp')
    tentss_danhsach = fields.Many2one(comodel_name='listtts.listtts', string='Tên thực tập sinh')
    danhsach_tts = fields.Many2one(comodel_name='nhapdiem.nhapdiem')
    tengiaovien = fields.Many2one(comodel_name='res.users', string='Tên giáo viên',
                                  related="danhsach_tts.giaovienchunhiem_nhapdiem")
    kha = fields.Integer(string="Khá giỏi", related='danhsach_tts.kha')
    trungbinh = fields.Integer(string="Trung bình", related='danhsach_tts.trungbinh')
    yeu = fields.Integer(string="Yếu", related='danhsach_tts.yeu')

    chua_codiem = fields.Boolean(string="Chưa có điểm",compute='cal_tong_danhgia')

    tuan = fields.Date(string="Ngày tạo", related='danhsach_tts.tuan')
    trungtam_danhsach = fields.Many2one(comodel_name='trungtam.daotao', string='Trung tâm đào tạo',
                                        related='danhsach_tts.trungtam_nhapdiem')
    ngaybc = fields.Date(string="Ngày tạo",)

    tuanbc = fields.Char('Tuần báo cáo', compute="_tuanbc", store=True)
    tuan_danhsach = fields.Selection(string="Tuần", selection=[('1', 'Tuần 1'), ('2', 'Tuần 2'), ('3', 'Tuần 3'),
                                                               ('4', 'Tuần 4'), ('5', 'Vỡ lòng')])

    @api.depends('ngaybc')
    def _tuanbc(self):
        for i in self:
            if i.ngaybc:
                tuan = i.ngaybc.isocalendar()[1]
                i.tuanbc = u'%s/%s' % (tuan, i.ngaybc.year)

    kha_b = fields.Boolean(string='Khá giỏi', compute='cal_tong_danhgia')
    trungbinh_b = fields.Boolean(string='Trung bình', compute='cal_tong_danhgia')
    yeukem_b = fields.Boolean(string='Yếu', compute='cal_tong_danhgia')
    ca_biet = fields.Boolean(string="Cá biệt", compute='cal_tong_danhgia')
    danhsach_thuctapsinh_volong = fields.Many2one(comodel_name='lop.thuctapsinh')
    danhsach_thuctapsinh = fields.Many2one(comodel_name='lop.thuctapsinh')
    danhgia_baihoc = fields.Many2one(comodel_name='lop.thuctapsinh')
    thang_danhgia = fields.Selection(string="Tháng", selection=[('1', 'Tháng 1'), ('2', 'Tháng 2'), ('3', 'Tháng 3'),
                                                                ('4', 'Tháng 4'), ('5', 'Tháng 5'), ('6', 'Tháng 6'),
                                                                ('7', 'Tháng 7'), ('8', 'Tháng 8'), ('9', 'Tháng 9'),
                                                                ('10', 'Tháng 10'), ('11', 'Tháng 11'),
                                                                ('12', 'Tháng 12')])
    nam_danhgia = fields.Char(string="Năm")
    danhgia_hoctap = fields.Char(string='Đánh giá học tập')
    danhgia_ythuc = fields.Char(string='Đánh giá ý thức')
    phuongan_xuly = fields.Char(string='Phương án xử lý')

    baihoc_danhgia = fields.Many2one('baihoc', 'Bài học')

    tuvung_1 = fields.Integer('Từ Vựng', default=0, store=True)
    nguphap = fields.Integer(string='Ngữ pháp', default=0, store=True)
    nghe = fields.Integer(string='Nghe', default=0, store=True)
    hoithoai = fields.Integer(string='Hội thoại', default=0, store=True)
    tiengnhat = fields.Integer(string='Đời sống', default=0, store=True)
    tong = fields.Integer(string='Tổng(500)', store=True, compute='cal_tong_danhgia')
    diem_sosanh = fields.Float('Điểm trung bình', store=True, compute='cal_tong_danhgia')
    xeploai = fields.Char('Xếp loại học tập',compute='cal_tong_danhgia')

    @api.multi
    @api.depends('tuvung_1', 'nguphap', 'nghe', 'hoithoai', 'tiengnhat', 'kha', 'trungbinh', 'yeu')
    def cal_tong_danhgia(self):
        for rec in self:
            if rec.nguphap > 100 or rec.nghe > 100 or rec.hoithoai > 100 or rec.tiengnhat > 100 or rec.tuvung_1 > 100:
                raise ValidationError(u"Điểm phải nhỏ hơn hoặc bằng 100")
            elif rec.nguphap < 0 or rec.nghe < 0 or rec.hoithoai < 0 or rec.tiengnhat < 0 or rec.tuvung_1 < 0:
                raise ValidationError(u"Điểm phải lớn hơn hoặc bằng 0")
            elif rec.nguphap <= 100 or rec.nghe <= 100 or rec.hoithoai <= 100 or rec.tiengnhat <= 100 or rec.tuvung_1 <= 100:
                rec.tong = rec.tuvung_1 + rec.nguphap + rec.nghe + rec.hoithoai + rec.tiengnhat
                dem = 0
                if rec.tuvung_1:
                    dem += 1
                if rec.nguphap:
                    dem += 1
                if rec.nghe:
                    dem += 1
                if rec.hoithoai:
                    dem += 1
                if rec.tiengnhat:
                    dem += 1

                if dem > 0:
                    rec.diem_sosanh = round(float(rec.tong) / float(dem))

                if rec.diem_sosanh > 0:
                    if rec.diem_sosanh >= rec.kha:
                        rec.kha_b = True
                        rec.xeploai = 'Khá Giỏi'
                    elif rec.diem_sosanh < rec.kha and rec.diem_sosanh >= rec.trungbinh:
                        rec.trungbinh_b = True
                        rec.xeploai = 'Trung Bình'
                    elif rec.diem_sosanh < rec.trungbinh and rec.diem_sosanh >= rec.yeu:
                        rec.yeukem_b = True
                        rec.xeploai = 'Yếu'
                    elif rec.diem_sosanh < rec.yeu and rec.diem_sosanh > 0:
                        rec.ca_biet = True
                        rec.xeploai = 'Kém'
                elif rec.diem_sosanh == 0 or rec.diem_sosanh == False:
                    rec.chua_codiem = True
                    rec.xeploai = 'Chưa Có Điểm'
                else:
                    rec.chua_codiem = False
    # ----------vỡ lòng -----------------------------------------------------
    do_volong = fields.Boolean('Đỗ', compute='cal_do_volong')
    volong_volong = fields.Boolean('Vỡ lòng')
    tuvung_volong = fields.Integer(string='Hiragana')
    nguphap_volong = fields.Integer(string='Katakana')

    tong_volong = fields.Integer(string='Tổng(200)', compute='cal_total_tong_volong', store=True)

    @api.multi
    @api.onchange('tuvung_volong', 'nguphap_volong')
    @api.depends('tuvung_volong', 'nguphap_volong')
    def cal_total_tong_volong(self):
        for tong in self:
            if tong.nguphap_volong > 100 or tong.tuvung_volong > 100:
                raise ValidationError(u"Điểm phải nhỏ hơn hoặc bằng 100")
            elif tong.nguphap_volong < 0 or tong.tuvung_volong < 0:
                raise ValidationError(u"Điểm phải lớn hơn 0")
            elif tong.tuvung_volong <= 100 and tong.nguphap_volong <= 100:
                tong.tong_volong = round(tong.tuvung_volong + tong.nguphap_volong)
            else:
                tong.tong_volong = 0

    @api.multi
    @api.depends('tuvung_volong', 'nguphap_volong')
    def cal_do_volong(self):
        for rc in self:
            if rc.tuvung_volong >= 70 and rc.nguphap_volong >= 70:
                rc.do_volong = True
            else:
                rc.do_volong = False

    ngaynghi = fields.Integer('Ngày Nghỉ')
    xl_ythuc = fields.Char('Xếp loại ý thức')

    def hienoccho(self):
        for i in self:
            if i.volong_volong == True:
                i.danhsach_thuctapsinh = []
            else:
                i.danhsach_thuctapsinh_volong = []

class LydoDanhgia(models.Model):
    _name = 'lydodanhgia'
    _rec_name = 'name'

    name = fields.Char('Lý do')
    sequence = fields.Integer('sequence', help='Sequence for the handle.')


from odoo import models, fields, api
import datetime
import logging
import codecs
from tempfile import TemporaryFile, NamedTemporaryFile
from io import BytesIO, StringIO
from docxtpl import DocxTemplate, Listing

_logger = logging.getLogger(__name__)


def month(day=None):
    if day:
        return u'%s' % (day[5:7])


def year(day=None):
    if day:
        return u'%s' % (day[0:4])


def date_time_in_jp(day=None, month=None, year=None):
    if not day:
        if not month:
            return u'%s年' % year
        else:
            return u'%s年%s月' % (year, month)
    else:
        return u'%s年%s月%s日' % (year, month, day)


class trungtamdaotao(models.Model):
    _name = 'trungtam.daotao'
    _rec_name = 'name_trungtam'

    name_trungtam = fields.Char('Tên trung tâm')


class TaoLop(models.Model):
    _name = 'taolop.taolop'
    _rec_name = 'tenlop_taolop'

    tenlop_taolop = fields.Char(string='Tên lớp', required=True)
    trungtam_taolop = fields.Many2one(comodel_name='trungtam.daotao', string='Trung tâm đào tạo')
    danhsach_thuctapsinh = fields.Many2many(comodel_name='listtts.listtts', string='Danh sách thực tập sinh')
    trangthai = fields.Selection(selection=[('1', 'Đang học'), ('2', 'Dừng học'), ('3', 'Đã bay')], string='Trạng thái',
                                 default='1')
    tong_songuoi = fields.Integer(string='Tổng số người', default=-1)

    giaovienchunhiem = fields.Many2one('res.users', 'Giáo viên chủ nhiệm')
    chuyen_lop = fields.Many2one('taolop.taolop', 'Chọn lớp chuyển đến')
    demo = fields.Boolean('Chuyển Lớp')

    @api.multi
    def confirm_chuyenlop(self):
        if self.chuyen_lop:
            lopchuyen_den = self.env['taolop.taolop'].search([('id', '=', self.chuyen_lop.id)])
            for tts in self.danhsach_thuctapsinh:
                danhsach_tts = self.env['lop.thuctapsinh'].search([('tentss_thuctapsinh', '=', tts.id)])
                if tts.chuyenlop == True:
                    self.danhsach_thuctapsinh = [(3, tts.id)]
                    tts.write({'chuyenlop': False})
                    danhsach_tts.write({'tenlop_thuctapsinh': lopchuyen_den.id})
                    lopchuyen_den.danhsach_thuctapsinh = [(4, tts.id)]
        self.demo = False
        self.chuyen_lop = ''

    @api.multi
    @api.onchange('demo')
    def onchange_demo_chuyenlop(self):
        if self.demo == True:
            for rec in self.danhsach_thuctapsinh:
                rec.chuyenlop == False

    @api.model
    def create(self, vals):
        danhsach = super(TaoLop, self).create(vals)
        if danhsach.danhsach_thuctapsinh:
            for danhsach_tts in danhsach.danhsach_thuctapsinh:
                danhsach_tts.lop_hoc = danhsach.id
                thuctapsinh = self.env['lop.thuctapsinh'].search([('tentss_thuctapsinh', '=', danhsach_tts.id)])
                if thuctapsinh:
                    thuctapsinh.write({'tentss_thuctapsinh': danhsach_tts.id, 'tenlop_thuctapsinh': danhsach.id})
                else:
                    thuctapsinh = self.env['lop.thuctapsinh'].create(
                        {'tentss_thuctapsinh': danhsach_tts.id, 'tenlop_thuctapsinh': danhsach.id})
        return danhsach

    @api.multi
    def write(self, vals):
        danhsach = super(TaoLop, self).write(vals)
        if self.danhsach_thuctapsinh:
            for danhsach_tts in self.danhsach_thuctapsinh:
                danhsach_tts.lop_hoc = self.id
                thuctapsinh = self.env['lop.thuctapsinh'].search([('tentss_thuctapsinh', '=', danhsach_tts.id)])
                if thuctapsinh:
                    thuctapsinh.write({'tentss_thuctapsinh': danhsach_tts.id, 'tenlop_thuctapsinh': self.id})
                else:
                    thuctapsinh = self.env['lop.thuctapsinh'].create(
                        {'tentss_thuctapsinh': danhsach_tts.id, 'tenlop_thuctapsinh': self.id})
        return danhsach

    @api.multi
    def write(self, vals):
        danhsach = super(TaoLop, self).write(vals)
        hientts = self.env['lop.thuctapsinh'].search([('tenlop_thuctapsinh', '=', self.id)])
        demo = []
        for i in hientts:
            demo.append(i.tentss_thuctapsinh)
        if self.danhsach_thuctapsinh:
            for danhsach_tts in self.danhsach_thuctapsinh:
                if danhsach_tts in demo:
                    demo.remove(danhsach_tts)
                danhsach_tts.lop_hoc = self.id
                thuctapsinh = self.env['lop.thuctapsinh'].search([('tentss_thuctapsinh', '=', danhsach_tts.id)])
                if thuctapsinh:
                    thuctapsinh.write({'tentss_thuctapsinh': danhsach_tts.id, 'tenlop_thuctapsinh': self.id})
                else:
                    thuctapsinh = self.env['lop.thuctapsinh'].create(
                        {'tentss_thuctapsinh': danhsach_tts.id, 'tenlop_thuctapsinh': self.id})
        for item in hientts:
            if item.tentss_thuctapsinh in demo:
                item.tenlop_thuctapsinh = ''
                item.tentss_thuctapsinh.lop_hoc = ''
        return danhsach


class xuatbaocao(models.Model):
    _name = 'xuatbaocao'
    _rec_name = 'ten_trungtam'

    ten_trungtam = fields.Many2one(comodel_name='trungtam.daotao', string='Trung tâm')
    tenlop = fields.Many2one(comodel_name='taolop.taolop', string='Tên lớp')
    phongban = fields.Many2one(comodel_name='department', string='Phòng ban')

    @api.multi
    @api.onchange('phongban')
    def domain_department(self):
        if self.phongban:
            return {'domain': {'nghiepdoan': [('room_pttt', '=', self.phongban.id)]}}

    nghiepdoan = fields.Many2one(comodel_name='nghiepdoan', string='Tên nghiệp đoàn')

    @api.multi
    @api.onchange('nghiepdoan')
    def domain_nghiepdoan(self):
        if self.nghiepdoan:
            return {'domain': {'xinghiep': [('nghiepdoan', '=', self.nghiepdoan.id)]}}

    xinghiep = fields.Many2one(comodel_name='xinghiep', string='Tên xí nghiệp')

    thuctapsinh_doingoai = fields.Many2many(comodel_name='lop.thuctapsinh', string='Danh sách thực tập sinh',
                                            compute="onchange_xuatbaocao")

    @api.multi
    @api.onchange('ten_trungtam', 'nghiepdoan', 'xinghiep', 'tenlop', 'phongban')
    def onchange_xuatbaocao(self):
        related_ids = []
        if self.ten_trungtam:
            if self.tenlop:
                if self.nghiepdoan:
                    # Shearch danh sach thuc tap sinh lop.thuctapsinh dang theo hoc
                    thuctapsinh_doingoai = self.env['lop.thuctapsinh'].search(
                        [('trungtam_lop', '=', self.ten_trungtam.id),
                         ('trangthai', 'in', ['1', '2']),
                         ('tenlop_thuctapsinh', '=', self.tenlop.id),
                         ('nghiepdoan_tiepnhan_char', '=', self.nghiepdoan.id)], order="xinghiep_tiepnhan asc")
                    # Append danh sach thuc tap sinh dang theo hoc
                    for doingoai in thuctapsinh_doingoai:
                        related_ids.append(doingoai.id)
                    # Gan vao invoices_promoted
                    self.thuctapsinh_doingoai = self.env['lop.thuctapsinh'].search([('id', 'in', related_ids)])

                elif self.xinghiep:
                    # Shearch danh sach thuc tap sinh lop.thuctapsinh dang theo hoc
                    thuctapsinh_doingoai = self.env['lop.thuctapsinh'].search(
                        [('trungtam_lop', '=', self.ten_trungtam.id),
                         ('trangthai', 'in', ['1', '2']),
                         ('tenlop_thuctapsinh', '=', self.tenlop.id),
                         ('xinghiep_tiepnhan', '=', self.xinghiep.id)], order="xinghiep_tiepnhan asc")
                    # Append danh sach thuc tap sinh dang theo hoc
                    for doingoai in thuctapsinh_doingoai:
                        related_ids.append(doingoai.id)
                    # Gan vao invoices_promoted
                    self.thuctapsinh_doingoai = self.env['lop.thuctapsinh'].search([('id', 'in', related_ids)])

                else:
                    # Shearch danh sach thuc tap sinh lop.thuctapsinh dang theo hoc
                    thuctapsinh_doingoai = self.env['lop.thuctapsinh'].search(
                        [('trungtam_lop', '=', self.ten_trungtam.id),
                         ('trangthai', 'in', ['1', '2']),
                         ('tenlop_thuctapsinh', '=', self.tenlop.id)], order="nghiepdoan_tiepnhan_char asc")
                    # Append danh sach thuc tap sinh dang theo hoc
                    for doingoai in thuctapsinh_doingoai:
                        related_ids.append(doingoai.id)
                    # Gan vao invoices_promoted
                    self.thuctapsinh_doingoai = self.env['lop.thuctapsinh'].search([('id', 'in', related_ids)])

            elif self.nghiepdoan:
                if self.tenlop:
                    # Shearch danh sach thuc tap sinh lop.thuctapsinh dang theo hoc
                    thuctapsinh_doingoai = self.env['lop.thuctapsinh'].search(
                        [('trungtam_lop', '=', self.ten_trungtam.id),
                         ('trangthai', 'in', ['1', '2']),
                         ('tenlop_thuctapsinh', '=', self.tenlop.id),
                         ('nghiepdoan_tiepnhan', '=', self.nghiepdoan.id)], order="xinghiep_tiepnhan asc")
                    # Append danh sach thuc tap sinh dang theo hoc
                    for doingoai in thuctapsinh_doingoai:
                        related_ids.append(doingoai.id)
                    # Gan vao invoices_promoted
                    self.thuctapsinh_doingoai = self.env['lop.thuctapsinh'].search([('id', 'in', related_ids)])

                elif self.xinghiep:
                    # Shearch danh sach thuc tap sinh lop.thuctapsinh dang theo hoc
                    thuctapsinh_doingoai = self.env['lop.thuctapsinh'].search(
                        [('trungtam_lop', '=', self.ten_trungtam.id),
                         ('trangthai', 'in', ['1', '2']),
                         ('nghiepdoan_tiepnhan', '=', self.nghiepdoan.id),
                         ('xinghiep_tiepnhan', '=', self.xinghiep.id)], order="xinghiep_tiepnhan asc")
                    # Append danh sach thuc tap sinh dang theo hoc
                    for doingoai in thuctapsinh_doingoai:
                        related_ids.append(doingoai.id)
                    # Gan vao invoices_promoted
                    self.thuctapsinh_doingoai = self.env['lop.thuctapsinh'].search([('id', 'in', related_ids)])

                else:
                    # Shearch danh sach thuc tap sinh lop.thuctapsinh dang theo hoc
                    thuctapsinh_doingoai = self.env['lop.thuctapsinh'].search(
                        [('trungtam_lop', '=', self.ten_trungtam.id),
                         ('trangthai', 'in', ['1', '2']),
                         ('nghiepdoan_tiepnhan', '=', self.nghiepdoan.id)], order="xinghiep_tiepnhan asc")
                    # Append danh sach thuc tap sinh dang theo hoc
                    for doingoai in thuctapsinh_doingoai:
                        related_ids.append(doingoai.id)
                    # Gan vao invoices_promoted
                    self.thuctapsinh_doingoai = self.env['lop.thuctapsinh'].search([('id', 'in', related_ids)])

            elif self.xinghiep:
                if self.tenlop:
                    # Shearch danh sach thuc tap sinh lop.thuctapsinh dang theo hoc
                    thuctapsinh_doingoai = self.env['lop.thuctapsinh'].search(
                        [('trungtam_lop', '=', self.ten_trungtam.id),
                         ('trangthai', 'in', ['1', '2']),
                         ('tenlop_thuctapsinh', '=', self.tenlop.id),
                         ('xinghiep_tiepnhan', '=', self.xinghiep.id)], order="xinghiep_tiepnhan asc")
                    # Append danh sach thuc tap sinh dang theo hoc
                    for doingoai in thuctapsinh_doingoai:
                        related_ids.append(doingoai.id)
                    # Gan vao invoices_promoted
                    self.thuctapsinh_doingoai = self.env['lop.thuctapsinh'].search([('id', 'in', related_ids)])

                else:
                    thuctapsinh_doingoai = self.env['lop.thuctapsinh'].search(
                        [('trungtam_lop', '=', self.ten_trungtam.id),
                         ('trangthai', 'in', ['1', '2']),
                         ('xinghiep_tiepnhan', '=', self.xinghiep.id)], order="xinghiep_tiepnhan asc")
                    # Append danh sach thuc tap sinh dang theo hoc
                    for doingoai in thuctapsinh_doingoai:
                        related_ids.append(doingoai.id)
                    # Gan vao invoices_promoted
                    self.thuctapsinh_doingoai = self.env['lop.thuctapsinh'].search([('id', 'in', related_ids)])

            elif self.phongban:
                if self.nghiepdoan:
                    # Shearch danh sach thuc tap sinh lop.thuctapsinh dang theo hoc
                    thuctapsinh_doingoai = self.env['lop.thuctapsinh'].search(
                        [('trungtam_lop', '=', self.ten_trungtam.id),
                         ('trangthai', 'in', ['1', '2']),
                         ('phongban_id', '=', self.phongban.id),
                         ('nghiepdoan_tiepnhan', '=', self.nghiepdoan.id)], order="xinghiep_tiepnhan asc")
                    # Append danh sach thuc tap sinh dang theo hoc
                    for doingoai in thuctapsinh_doingoai:
                        related_ids.append(doingoai.id)
                    # Gan vao invoices_promoted
                    self.thuctapsinh_doingoai = self.env['lop.thuctapsinh'].search([('id', 'in', related_ids)])

                elif self.xinghiep:
                    # Shearch danh sach thuc tap sinh lop.thuctapsinh dang theo hoc
                    thuctapsinh_doingoai = self.env['lop.thuctapsinh'].search(
                        [('trungtam_lop', '=', self.ten_trungtam.id),
                         ('trangthai', 'in', ['1', '2']),
                         ('phongban_id', '=', self.phongban.id),
                         ('xinghiep_tiepnhan', '=', self.xinghiep.id)], order="xinghiep_tiepnhan asc")
                    # Append danh sach thuc tap sinh dang theo hoc
                    for doingoai in thuctapsinh_doingoai:
                        related_ids.append(doingoai.id)
                    # Gan vao invoices_promoted
                    self.thuctapsinh_doingoai = self.env['lop.thuctapsinh'].search([('id', 'in', related_ids)])

                else:
                    thuctapsinh_doingoai = self.env['lop.thuctapsinh'].search(
                        [('trungtam_lop', '=', self.ten_trungtam.id),
                         ('trangthai', 'in', ['1', '2']),
                         ('phongban_id', '=', self.phongban.id)], order="ghiepdoan_tiepnhan_char asc")
                    # Append danh sach thuc tap sinh dang theo hoc
                    for doingoai in thuctapsinh_doingoai:
                        related_ids.append(doingoai.id)
                    # Gan vao invoices_promoted
                    self.thuctapsinh_doingoai = self.env['lop.thuctapsinh'].search([('id', 'in', related_ids)])

            else:
                # Shearch danh sach thuc tap sinh lop.thuctapsinh dang theo hoc
                thuctapsinh_doingoai = self.env['lop.thuctapsinh'].search(
                    [('trungtam_lop', '=', self.ten_trungtam.id), ('trangthai', 'in', ['1', '2'])],
                    order="nghiepdoan_tiepnhan asc")
                # Append danh sach thuc tap sinh dang theo hoc
                for doingoai in thuctapsinh_doingoai:
                    related_ids.append(doingoai.id)
                # Gan vao invoices_promoted
                self.thuctapsinh_doingoai = self.env['lop.thuctapsinh'].search([('id', 'in', related_ids)])

        if self.nghiepdoan and self.ten_trungtam == False:
            # Shearch danh sach thuc tap sinh lop.thuctapsinh dang theo hoc
            thuctapsinh_doingoai = self.env['lop.thuctapsinh'].search(
                [('nghiepdoan_tiepnhan', '=', self.nghiepdoan.id), ('trangthai', 'in', ['1', '2'])],
                order="xinghiep_tiepnhan asc")
            # Append danh sach thuc tap sinh dang theo hoc
            for doingoai in thuctapsinh_doingoai:
                related_ids.append(doingoai.id)
            # Gan vao invoices_promoted
            self.thuctapsinh_doingoai = self.env['lop.thuctapsinh'].search([('id', 'in', related_ids)])

        if self.xinghiep and self.ten_trungtam == False:
            # Shearch danh sach thuc tap sinh lop.thuctapsinh dang theo hoc
            thuctapsinh_doingoai = self.env['lop.thuctapsinh'].search(
                [('xinghiep_tiepnhan', '=', self.xinghiep.id), ('trangthai', 'in', ['1', '2'])],
                order="xinghiep_tiepnhan asc")
            # Append danh sach thuc tap sinh dang theo hoc
            for doingoai in thuctapsinh_doingoai:
                related_ids.append(doingoai.id)
            # Gan vao invoices_promoted
            self.thuctapsinh_doingoai = self.env['lop.thuctapsinh'].search([('id', 'in', related_ids)])

        if self.phongban and self.ten_trungtam == False:
            # Shearch danh sach thuc tap sinh lop.thuctapsinh dang theo hoc
            thuctapsinh_doingoai = self.env['lop.thuctapsinh'].search(
                [('phongban_id', '=', self.phongban.id), ('trangthai', 'in', ['1', '2'])],
                order="nghiepdoan_tiepnhan asc")
            # Append danh sach thuc tap sinh dang theo hoc
            for doingoai in thuctapsinh_doingoai:
                related_ids.append(doingoai.id)
            # Gan vao invoices_promoted
            self.thuctapsinh_doingoai = self.env['lop.thuctapsinh'].search([('id', 'in', related_ids)])

        if self.tenlop and self.ten_trungtam == False:
            # Shearch danh sach thuc tap sinh lop.thuctapsinh dang theo hoc
            thuctapsinh_doingoai = self.env['lop.thuctapsinh'].search(
                [('tenlop_thuctapsinh', '=', self.tenlop.id), ('trangthai', 'in', ['1', '2'])],
                order="nghiepdoan_tiepnhan asc")
            # Append danh sach thuc tap sinh dang theo hoc
            for doingoai in thuctapsinh_doingoai:
                related_ids.append(doingoai.id)
            # Gan vao invoices_promoted
            self.thuctapsinh_doingoai = self.env['lop.thuctapsinh'].search([('id', 'in', related_ids)])

    @api.multi
    def baocao_anhnguyen(self):
        return {
            'type': 'ir.actions.act_url',
            'url': '/xuatbaocao/%s' % (self.id),
            'target': 'self', }

    def baocaoanhnguyen(self, document, dataid):
        docs = self.env['intern.document'].search([('name', '=', "InfoTTS")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].attachment, "base64"))
            docdata = DocxTemplate(stream)
            context = {}
            data = self.env['lop.thuctapsinh'].search([('id', '=', dataid)])

            if data.tentss_thuctapsinh.avatar is not None:
                try:
                    streamAvatar = BytesIO(data.tentss_thuctapsinh.avatar.decode("base64"))
                    docdata.replace_pic('avatar.jpg', streamAvatar)
                except:
                    pass

            context[
                'hhjp_0001'] = data.tentss_thuctapsinh.name_in_japan if data.tentss_thuctapsinh.name_in_japan else ''
            context[
                'hhjp_0097'] = data.nghiepdoan_tiepnhan.name_china if data.nghiepdoan_tiepnhan.name_china else ''
            hhjp_0010 = data.xinghiep_tiepnhan.name_china if data.xinghiep_tiepnhan.name_china else ''
            hhjp_0010_1 = data.place_to_work if data.place_to_work else ''
            if data.xinghiep_tiepnhan.name_china:
                context['hhjp_0010'] = hhjp_0010
            elif data.place_to_work:
                context['hhjp_0010'] = hhjp_0010_1
            elif data.xinghiep_tiepnhan.name_china and data.place_to_work:
                context['hhjp_0010'] = '%s / %s' + (hhjp_0010, hhjp_0010_1)
            else:
                context['hhjp_0010'] = '*'

            context['hhjp_0115'] = data.job_jp.name if data.job_jp.name else ''
            # Ngày tháng năm sinh
            if data.tentss_thuctapsinh.date_of_birth:
                context['hhjp_0004'] = date_time_in_jp(data.tentss_thuctapsinh.date_of_birth.day,
                                                       data.tentss_thuctapsinh.date_of_birth.month,
                                                       data.tentss_thuctapsinh.date_of_birth.year)
            else:
                context['hhjp_0004'] = u'年　月　日'
            context[
                'csdt_0001'] = data.tenlop_thuctapsinh.tenlop_taolop if data.tenlop_thuctapsinh.tenlop_taolop else ''
            context['csdt_0003'] = data.chunhiem_lop.partner_id.name if data.chunhiem_lop.partner_id.name else ''

            if data.batdau_lop:
                context['csdt_0002'] = date_time_in_jp(data.batdau_lop.day,
                                                       data.batdau_lop.month,
                                                       data.batdau_lop.year)
            else:
                context['csdt_0002'] = u'年　月　日'

            if data.lichhoc_lop:
                context['hhjp_0902'] = date_time_in_jp(data.lichhoc_lop.day,
                                                       data.lichhoc_lop.month,
                                                       data.lichhoc_lop.year)
            else:
                context['hhjp_0902'] = u'年　月　日'
            diem_volong = 0
            sosanh = 0
            for rec in data.danhsach_thuctapsinh_volong:
                sosanh = 0 + diem_volong
                diem_volong = rec.tong_volong
                if diem_volong >= sosanh:
                    context['csdt_0004'] = rec.tuvung_volong if rec.tuvung_volong else '*'
                    context['csdt_0005'] = rec.nguphap_volong if rec.nguphap_volong else '*'
                    context['csdt_0009'] = rec.tong_volong if rec.tong_volong else '*'
                    if rec.do_volong == True:
                        context['csdt_0010'] = u'✔'
                        context['csdt_0011'] = u''
                    else:
                        context['csdt_0010'] = u''
                        context['csdt_0011'] = u'✔'
            context['csdt_0012'] = data.thoigian_danhgia if data.thoigian_danhgia else '*'
            context['csdt_0013'] = data.suckhoe_danhgia if data.suckhoe_danhgia else '*'
            context['csdt_0014'] = data.kiluat_danhgia if data.kiluat_danhgia else '*'

            danhgia_ythuctot_volong = ''
            for item in data.danhgia_ythuctot_volong:
                danhgia_ythuctot_volong = danhgia_ythuctot_volong + '\n' + u'・' + (item.name)

            danhgia_ythucxau_volong = ''
            for item in data.danhgia_ythucxau_volong:
                danhgia_ythucxau_volong = danhgia_ythucxau_volong + '\n' + u'・' + (item.name)

            if data.danhgia_ythuctot_volong and data.danhgia_ythucxau_volong:
                danhgia = danhgia_ythuctot_volong + ' %s' % (
                    danhgia_ythucxau_volong)
            elif data.danhgia_ythuctot_volong:
                danhgia = danhgia_ythuctot_volong
            elif data.danhgia_ythucxau_volong:
                danhgia = danhgia_ythucxau_volong
            else:
                danhgia = '*'
            context['csdt_0015'] = Listing(danhgia)

            danhgia_volong = ''
            for item in data.danhgia_volong:
                danhgia_volong = danhgia_volong + '\n' + u'・' + (item.name)
            context['csdt_0016'] = Listing(danhgia_volong)

            table_daotao = []
            thang = []
            for danhgia_nangluc in data.thaido_tts_lop:
                if danhgia_nangluc.thang not in thang:
                    thang.append(danhgia_nangluc.thang)
                    infor_danhgia_nangluc = {}
                    infor_danhgia_nangluc[
                        'csdt_0019'] = danhgia_nangluc.thang if danhgia_nangluc.thang else ''
                    table_baihoc_thang = []
                    item = 1
                    for baihoc_thang in data.danhgia_nangluc_tts_lop:
                        if baihoc_thang.thang_danhgia == danhgia_nangluc.thang:
                            if item == 1:
                                infor_danhgia_nangluc[
                                    'csdt_0020_1'] = baihoc_thang.baihoc_danhgia.baihocdanhgia if baihoc_thang.baihoc_danhgia.baihocdanhgia else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0021_1'] = baihoc_thang.tuvung_1 if baihoc_thang.tuvung_1 else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0022_1'] = baihoc_thang.nguphap if baihoc_thang.nguphap else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0023_1'] = baihoc_thang.nghe if baihoc_thang.nghe else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0024_1'] = baihoc_thang.hoithoai if baihoc_thang.hoithoai else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0025_1'] = baihoc_thang.tiengnhat if baihoc_thang.tiengnhat else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0026_1'] = baihoc_thang.tong if baihoc_thang.tong else '*'
                                item += 1;
                            elif item == 2:
                                infor_danhgia_nangluc[
                                    'csdt_0020_2'] = baihoc_thang.baihoc_danhgia.baihocdanhgia if baihoc_thang.baihoc_danhgia.baihocdanhgia else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0021_2'] = baihoc_thang.tuvung_1 if baihoc_thang.tuvung_1 else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0022_2'] = baihoc_thang.nguphap if baihoc_thang.nguphap else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0023_2'] = baihoc_thang.nghe if baihoc_thang.nghe else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0024_2'] = baihoc_thang.hoithoai if baihoc_thang.hoithoai else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0025_2'] = baihoc_thang.tiengnhat if baihoc_thang.tiengnhat else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0026_2'] = baihoc_thang.tong if baihoc_thang.tong else '*'
                                item += 1;
                            elif item == 3:
                                infor_danhgia_nangluc[
                                    'csdt_0020_3'] = baihoc_thang.baihoc_danhgia.baihocdanhgia if baihoc_thang.baihoc_danhgia.baihocdanhgia else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0021_3'] = baihoc_thang.tuvung_1 if baihoc_thang.tuvung_1 else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0022_3'] = baihoc_thang.nguphap if baihoc_thang.nguphap else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0023_3'] = baihoc_thang.nghe if baihoc_thang.nghe else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0024_3'] = baihoc_thang.hoithoai if baihoc_thang.hoithoai else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0025_3'] = baihoc_thang.tiengnhat if baihoc_thang.tiengnhat else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0026_3'] = baihoc_thang.tong if baihoc_thang.tong else '*'
                                item += 1;
                            elif item == 4:
                                infor_danhgia_nangluc[
                                    'csdt_0020_4'] = baihoc_thang.baihoc_danhgia.baihocdanhgia if baihoc_thang.baihoc_danhgia.baihocdanhgia else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0021_4'] = baihoc_thang.tuvung_1 if baihoc_thang.tuvung_1 else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0022_4'] = baihoc_thang.nguphap if baihoc_thang.nguphap else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0023_4'] = baihoc_thang.nghe if baihoc_thang.nghe else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0024_4'] = baihoc_thang.hoithoai if baihoc_thang.hoithoai else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0025_4'] = baihoc_thang.tiengnhat if baihoc_thang.tiengnhat else '*'
                                infor_danhgia_nangluc[
                                    'csdt_0026_4'] = baihoc_thang.tong if baihoc_thang.tong else '*'
                                break

                    for thaido_tts_lop in data.thaido_tts_lop:
                        if thaido_tts_lop.thang == danhgia_nangluc.thang:
                            infor_danhgia_nangluc[
                                'csdt_0028'] = thaido_tts_lop.thoigian if thaido_tts_lop.thoigian else '*'
                            infor_danhgia_nangluc[
                                'csdt_0029'] = thaido_tts_lop.tinhthan if thaido_tts_lop.tinhthan else '*'
                            infor_danhgia_nangluc['csdt_0030'] = thaido_tts_lop.kiluat if thaido_tts_lop.kiluat else '*'
                            infor_danhgia_nangluc[
                                'csdt_0017'] = thaido_tts_lop.vang_danhgia if thaido_tts_lop.vang_danhgia else ''

                            lydo_danhgia = ''
                            for item in thaido_tts_lop.lydo_danhgia:
                                lydo_danhgia = lydo_danhgia + '\n' + u'・' + (item.name)
                            infor_danhgia_nangluc['csdt_0018'] = Listing(lydo_danhgia) if lydo_danhgia else '*'

                            nhanxet_hoctaptot = ''
                            for item in thaido_tts_lop.nhanxet_hoctaptot:
                                nhanxet_hoctaptot = nhanxet_hoctaptot + '\n' + u'・' + (item.name)
                            nhanxet_hoctapxau = ''
                            for item in thaido_tts_lop.nhanxet_hoctapxau:
                                nhanxet_hoctapxau = nhanxet_hoctapxau + '\n' + u'・' + (item.name)

                            if thaido_tts_lop.nhanxet_hoctaptot and thaido_tts_lop.nhanxet_hoctapxau:
                                danhgia_nangluc1 = nhanxet_hoctaptot + '%s' % (nhanxet_hoctapxau)
                            elif thaido_tts_lop.nhanxet_hoctaptot:
                                danhgia_nangluc1 = nhanxet_hoctaptot
                            elif thaido_tts_lop.nhanxet_hoctapxau:
                                danhgia_nangluc1 = nhanxet_hoctapxau
                            else:
                                danhgia_nangluc1 = '*'

                            infor_danhgia_nangluc['csdt_0027'] = Listing(danhgia_nangluc1)

                            nhanxet_ythuctot = ''
                            for item in thaido_tts_lop.nhanxet_ythuctot:
                                nhanxet_ythuctot = nhanxet_ythuctot + '\n' + u'・' + (item.name)

                            nhanxet_ythucxau = ''
                            for item in thaido_tts_lop.nhanxet_ythucxau:
                                nhanxet_ythucxau = nhanxet_ythucxau + '\n' + u'・' + (item.name)

                            if thaido_tts_lop.nhanxet_ythuctot and thaido_tts_lop.nhanxet_ythucxau:
                                csdt_0031 = nhanxet_ythuctot + u'・' + (nhanxet_ythucxau)
                            elif thaido_tts_lop.nhanxet_ythuctot:
                                csdt_0031 = nhanxet_ythuctot
                            elif thaido_tts_lop.nhanxet_ythucxau:
                                csdt_0031 = nhanxet_ythucxau
                            else:
                                csdt_0031 = '*'

                            infor_danhgia_nangluc['csdt_0031'] = Listing(csdt_0031)
                    table_daotao.append(infor_danhgia_nangluc)
            context['tbl_baihoc'] = table_daotao

            table_diem_n5 = []
            for i in data.diem_n5:
                diem = {}
                if i.thang_danhgia_n5:
                    diem['csdt_0041'] = date_time_in_jp(i.thang_danhgia_n5.day,
                                                        i.thang_danhgia_n5.month,
                                                        i.thang_danhgia_n5.year)
                else:
                    diem['csdt_0041'] = u'年　月　日'
                diem['csdt_0032'] = i.tuvung_lop if i.tuvung_lop else ''
                diem['csdt_0033'] = i.doc_nguphap_lop if i.doc_nguphap_lop else ''
                diem['csdt_0034'] = i.nghe_hieu_lop if i.nghe_hieu_lop else ''
                diem['csdt_0035'] = i.hoi_thoai_lop if i.hoi_thoai_lop else ''
                diem['csdt_0036'] = i.tong_lop if i.tong_lop else ''
                diem['csdt_0039'] = i.danhgia_n5 if i.danhgia_n5 else ''
                if i.tong_lop or i.thang_danhgia_n5:
                    if i.do_lop == True:
                        diem['csdt_0037'] = u'✔'
                    else:
                        diem['csdt_0038'] = u'✔'
                else:
                    diem['csdt_0037'] = ''
                    diem['csdt_0038'] = ''
                table_diem_n5.append(diem)
            context['tbl_n5'] = table_diem_n5

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.flush()
            tempFile.close()
            return tempFile
        return None

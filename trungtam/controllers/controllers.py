# -*- coding: utf-8 -*-
from odoo import http
from odoo.http import request
from odoo.addons.web.controllers.main import serialize_exception, content_disposition
from io import BytesIO, StringIO
import os
import zipfile


class TrungTam(http.Controller):
    @http.route('/trungtam/<string:id>', type='http', auth="public")
    def download(self, id=False, **kwargs):
        reponds = BytesIO()
        archive = zipfile.ZipFile(reponds, 'w', zipfile.ZIP_DEFLATED)
        document = request.env['lop.thuctapsinh'].browse(int(id))
        invoice = request.env['lop.thuctapsinh']
        finalDoc = invoice.baocao_hoso(document)
        demo = document.tentss_thuctapsinh.name
        phongban = document.phongban.name
        nghiepdoan = document.nghiepdoan_tiepnhan.name_roma if document.nghiepdoan_tiepnhan.name_roma else 'NONE'
        congty = document.xinghiep_tiepnhan.name_romaji if document.xinghiep_tiepnhan.name_romaji else 'NONE'
        diadiem_lamviec = document.place_to_work if document.place_to_work else 'NONE'
        nam_xuatcanh = document.thang_nam if document.thang_nam else 'NONE'
        archive.write(finalDoc.name,
                      r'%s \ %s \ %s \ %s \ %s \ %s.docx' % (
                          phongban, nghiepdoan, congty, diadiem_lamviec, nam_xuatcanh, demo))
        os.unlink(finalDoc.name)

        archive.close()
        reponds.flush()
        ret_zip = reponds.getvalue()
        reponds.close()

        finalDoc.name = 'HH_HOSO.zip'
        return request.make_response(ret_zip,
                                     [('Content-Type', 'application/zip'),
                                      ('Content-Disposition', content_disposition(finalDoc.name))])

    @http.route('/trungtam/noibo/<string:id>', type='http', auth="public")
    def downloadnoibo(self, id=False, **kwargs):
        reponds = BytesIO()
        archive = zipfile.ZipFile(reponds, 'w', zipfile.ZIP_DEFLATED)
        document = request.env['lop.thuctapsinh'].browse(int(id))
        invoice = request.env['lop.thuctapsinh']
        finalDoc = invoice.baocao_hoso_noibo(document)
        demo = document.tentss_thuctapsinh.name
        archive.write(finalDoc.name, r'%s.docx' % (demo))
        os.unlink(finalDoc.name)

        archive.close()
        reponds.flush()
        ret_zip = reponds.getvalue()
        reponds.close()

        finalDoc.name = 'HH_HOSO.zip'
        return request.make_response(ret_zip,
                                     [('Content-Type', 'application/zip'),
                                      ('Content-Disposition', content_disposition(finalDoc.name))])

    @http.route('/baocao_theotuan_button/<string:id>', type='http', auth="public")
    def download_baocao_theotuan_button(self, id=False, **kwargs):
        reponds = BytesIO()
        archive = zipfile.ZipFile(reponds, 'w', zipfile.ZIP_DEFLATED)
        document = request.env['nhapdiem.nhapdiem'].browse(int(id))
        invoice = request.env['nhapdiem.nhapdiem']
        if document.volong_nhapdiem == True:
            finalDoc = invoice.baocao_theotuan_volong(document)
            volong = document.tenlop_nhapdiem.tenlop_taolop
            archive.write(finalDoc.name, r'\%s.docx' % (volong))
            os.unlink(finalDoc.name)
        else:
            finalDoc = invoice.baocao_theotuan(document)
            demo = document.tenlop_nhapdiem.tenlop_taolop
            archive.write(finalDoc.name, r'\%s.docx' % (demo))
            os.unlink(finalDoc.name)
        archive.close()
        reponds.flush()
        ret_zip = reponds.getvalue()
        reponds.close()

        finalDoc.name = 'HH_HOSO.zip'
        return request.make_response(ret_zip,
                                     [('Content-Type', 'application/zip'),
                                      ('Content-Disposition', content_disposition(finalDoc.name))])

    @http.route('/baocao_theoquatrinh_button/<string:id>', type='http', auth="public")
    def download_baocao_theoquatrinh_button(self, id=False, **kwargs):
        reponds = BytesIO()
        archive = zipfile.ZipFile(reponds, 'w', zipfile.ZIP_DEFLATED)
        document = request.env['nhapdiem.nhapdiem'].browse(int(id))
        invoice = request.env['nhapdiem.nhapdiem']
        baihoc = document.danhsach_thuctapsinh
        volong1 = document.danhsach_thuctapsinh_volong
        if document.volong_nhapdiem == True:
            for thuctapsinh in volong1:
                finalDoc = invoice.baocao_hoso_quatrinh_volong(document, thuctapsinh.id)
                demo = thuctapsinh.tentss_danhsach.name
                phongban = thuctapsinh.danhsach_thuctapsinh_volong.phongban.name
                nghiepdoan = thuctapsinh.danhsach_thuctapsinh_volong.nghiepdoan_tiepnhan.name_roma if thuctapsinh.danhsach_thuctapsinh_volong.nghiepdoan_tiepnhan.name_roma else 'NONE'
                congty = thuctapsinh.danhsach_thuctapsinh_volong.xinghiep_tiepnhan.name_romaji if thuctapsinh.danhsach_thuctapsinh_volong.xinghiep_tiepnhan.name_romaji else 'NONE'
                nam_xuatcanh = thuctapsinh.danhsach_thuctapsinh_volong.thang_nam if thuctapsinh.danhsach_thuctapsinh_volong.thang_nam else 'NONE'
                archive.write(finalDoc.name,
                              r'%s \ %s \ %s \ %s \ %s.docx' % (
                                  phongban, nghiepdoan, congty, nam_xuatcanh, demo))
                os.unlink(finalDoc.name)
        else:
            for thuctapsinh in baihoc:
                finalDoc = invoice.baocao_hoso_quatrinh(document, thuctapsinh.id)
                demo = thuctapsinh.tentss_danhsach.name
                phongban = thuctapsinh.danhsach_thuctapsinh.phongban.name
                nghiepdoan = thuctapsinh.danhsach_thuctapsinh.nghiepdoan_tiepnhan.name_roma if thuctapsinh.danhsach_thuctapsinh.nghiepdoan_tiepnhan.name_roma else 'NONE'
                congty = thuctapsinh.danhsach_thuctapsinh.xinghiep_tiepnhan.name_romaji if thuctapsinh.danhsach_thuctapsinh.xinghiep_tiepnhan.name_romaji else 'NONE'
                nam_xuatcanh = thuctapsinh.danhsach_thuctapsinh.thang_nam if thuctapsinh.danhsach_thuctapsinh.thang_nam else 'NONE'
                archive.write(finalDoc.name, r'%s \ %s \ %s \ %s \ %s.docx' % (
                    phongban, nghiepdoan, congty, nam_xuatcanh, demo))
                os.unlink(finalDoc.name)

        archive.close()
        reponds.flush()
        ret_zip = reponds.getvalue()
        reponds.close()

        finalDoc.name = 'HH_HOSO.zip'
        return request.make_response(ret_zip,
                                     [('Content-Type', 'application/zip'),
                                      ('Content-Disposition', content_disposition(finalDoc.name))])

    @http.route('/baocao/diem/<string:id>', type='http', auth="public")
    def download_baocao_theotuan_diem(self, id=False, **kwargs):
        reponds = BytesIO()
        archive = zipfile.ZipFile(reponds, 'w', zipfile.ZIP_DEFLATED)
        document = request.env['baocao.lop'].browse(int(id))
        invoice = request.env['baocao.lop']

        demo = document.lop.tenlop_taolop

        return request.make_response(invoice.baocao_theotuan_diem(document),
                                     headers=[('Content-Disposition',
                                               content_disposition(r'%s.docx' % (demo))),
                                              ('Content-Type',
                                               'application/vnd.openxmlformats-officedocument.wordprocessingml.document')])

    @http.route('/diem/diem_trungtam/<string:id>', type='http', auth="public")
    def download_baocao_theotuan_trungtam_diem(self, id=False, **kwargs):
        reponds = BytesIO()
        archive = zipfile.ZipFile(reponds, 'w', zipfile.ZIP_DEFLATED)
        document = request.env['baocao.trungtam'].browse(int(id))
        invoice = request.env['baocao.trungtam']
        demo = document.ten_trungtam_1.name_trungtam

        return request.make_response(invoice.baocao_theotuan_trungtam_diem(document),
                                     headers=[('Content-Disposition',
                                               content_disposition(r'%s.docx' % (demo))),
                                              ('Content-Type',
                                               'application/vnd.openxmlformats-officedocument.wordprocessingml.document')])

    @http.route('/xuatbaocao/<string:id>', type='http', auth="public")
    def download_baocao_xuatbaocao_button(self, id=False, **kwargs):
        reponds = BytesIO()
        archive = zipfile.ZipFile(reponds, 'w', zipfile.ZIP_DEFLATED)
        document = request.env['xuatbaocao'].browse(int(id))
        invoice = request.env['xuatbaocao']
        data = document.thuctapsinh_doingoai
        if data:
            for thuctapsinh in data:
                finalDoc = invoice.baocaoanhnguyen(document, thuctapsinh.id)
                demo = 'cv_%s' % (thuctapsinh.tentss_thuctapsinh.name)
                phongban = thuctapsinh.phongban.name
                nghiepdoan = thuctapsinh.nghiepdoan_tiepnhan.name_roma if thuctapsinh.nghiepdoan_tiepnhan.name_roma else 'NONE'
                xinghiep = thuctapsinh.xinghiep_tiepnhan.name_romaji if thuctapsinh.xinghiep_tiepnhan.name_romaji else 'NONE'
                lop = thuctapsinh.tenlop_thuctapsinh.tenlop_taolop if thuctapsinh.tenlop_thuctapsinh.tenlop_taolop else 'NONE'
                archive.write(finalDoc.name,
                              r'%s \ %s \ %s \ %s \ %s.docx' % (
                                  phongban, nghiepdoan, xinghiep, lop, demo))
                os.unlink(finalDoc.name)

        archive.close()
        reponds.flush()
        ret_zip = reponds.getvalue()
        reponds.close()

        finalDoc.name = 'HH_HOSO.zip'
        return request.make_response(ret_zip,
                                     [('Content-Type', 'application/zip'),
                                      ('Content-Disposition', content_disposition(finalDoc.name))])
